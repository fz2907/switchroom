# This is the README file for SwitchRoom Application 

## There will be 4 folders in the root level:
- FrontendFolder: This folder will hold all the frontend file.
- BackendFolder: This folder will hold all the backend files.
- DataBaseFolder: This folder will hold all the DataBase related files.
- ScriptFolder: This folder will hold all the common script file that may use for deploy or build connections between projects.

## There are few rules about how to branch and merge the project:
- The main branch should only contain working code. So, all the changes will be made in new branches, and merge back after testing.
- If you want to make changes about a file:
   1. First, clone the project, make sure it can work properly on local.
   2. Branch the file from main, make changes on the new branch.
   3. Test the new change is working properly.
   4. Merge the file back to main. 
- Each git push should give enough information about the changes.

## Rule about coding style:
- New files should contain comment to explain the functionality in begin of the code file.
- Each function should have its own comment.
- Try to not write a super long function.
- Always add log in the code to help people to trace bug. 
- Use try and catch and give clear error code!