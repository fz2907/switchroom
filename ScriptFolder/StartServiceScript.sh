
#function to start VUE
startVUE(){
  echo "Starting VUE service..."
  # shellcheck disable=SC2164
  cd ../FrontendFolder/switch-room
  npm run serve
}

#function to start DB
startDB(){
  echo "Starting DB service..."
  # shellcheck disable=SC2164
  cd ../DataBaseFolder/MySQL
  docker-compose up
}

startDB