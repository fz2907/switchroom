import { defineStore } from "pinia";
import { FlightResultModel } from "@/models/FlightResultModel";
import { FlightFormModel } from "@/models/FlightFormModel";
import * as FlightTicketService from "@/services/FlightTicketService";

export const useTicketStore = defineStore("TicketStore", {
  //state
  //options
  //getters
  state: () => ({
    ticketList: [] as FlightResultModel[],
  }),
  actions: {
    async fetchResultTicket(flightModel: FlightFormModel) {
      await FlightTicketService.createNewFlight(flightModel).then(
        (response) => {
          console.log("Save search flight data: ", response.data);
          this.ticketList = response.data;
        }
      );
    },
  },
});
