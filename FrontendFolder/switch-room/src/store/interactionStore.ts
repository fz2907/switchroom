import { defineStore } from "pinia";
import { InteractionModel } from "@/models/InteractionModel";
import * as PhoneService from "@/services/PhoneService";

export const useInteractionStore = defineStore("InteractionStore", {
  //state
  //options
  //getters
  state: () => ({
    interactionList: [] as InteractionModel[],
  }),
  actions: {
    async fetchInteractionData(data: any) {
      await PhoneService.postPhoneToServer(data).then((response) => {
        console.log("fetched interaction data", response.data);
        this.interactionList = response.data;
        console.log(this.interactionList);
      });
    },
  },
});
