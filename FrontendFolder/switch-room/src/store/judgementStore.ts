import { defineStore } from "pinia";
import * as PhoneService from "@/services/PhoneService";

export const useJudgementStore = defineStore("JudgementStore", {
  //state
  //options
  //getters
  state: () => ({
    judgement: String,
  }),
  actions: {
    markJudgement(data: any) {
      this.judgement = data;
    },
  },
});
