import * as UserService from "../../services/UserService";

const state = () => ({
  loginStatus: false,
});

const getters = {
  getLoginStatus(state: any) {
    return state.loginStatus;
  },
};

const actions = {
  async loginApi({ commit }: any, payload: any) {
    const response = await UserService.loginUser(JSON.stringify(payload)).catch(
      (error) => alert(error)
    );
    if (response.status == "OK") {
      commit("setLoginStatus", true);
    } else {
      alert(response.message);
    }
  },
  logOutApi({ commit }: any) {
    // todo: clear session in backend
    console.log("logOut");
    commit("setLoginStatus", false);
  },
};

const mutations = {
  setLoginStatus(state: any, data: any) {
    state.loginStatus = data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
