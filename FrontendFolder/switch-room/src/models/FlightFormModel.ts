export class FlightFormModel {
  public departure: string;
  public destination: string;
  public date1: Date | string;
  public date2: Date | string;
  public numAdults: number;
  public numSeniors: number;
  public classOfService: string;

  constructor(
    departure = "",
    destination = "",
    date1 = new Date(),
    date2 = new Date(),
    numAdults = 1,
    numSeniors = 0,
    classOfService = ""
  ) {
    this.departure = departure;
    this.destination = destination;
    this.date1 = date1;
    this.date2 = date2;
    this.numAdults = numAdults;
    this.numSeniors = numSeniors;
    this.classOfService = classOfService;
  }
}
