export class UserModel {
  public readonly userId: number;
  public username: string;
  public password: string | null;
  public email: string;
  public firstname: string;
  public lastname: string;
  public gender: string;

  constructor(
    userId = -1,
    username = "",
    password = "",
    email = "",
    firstname = "",
    lastname = "",
    gender = "",
  ) {
    this.userId = userId;
    this.username = username;
    this.password = password;
    this.email = email;
    this.firstname = firstname;
    this.lastname = lastname;
    this.gender = gender;
  }
}
  