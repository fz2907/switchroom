export class OfferPageWishlistMatchRequestInfo {
    wishlistOwnerId: number;
    wishlistId: number;
    startTime: Date;
    endTime: Date;
    wishListOwnerName: string;
    hasOffer: boolean;
    zipCode: number;

    constructor(wishlistOwnerId = -1,
    wishlistId = -1,
    startTime = new Date(),
    endTime = new Date(),
    wishListOwnerName = "",
    hasOffer = false,
    zipCode = -1) {
        this.wishlistOwnerId = wishlistOwnerId;
        this.wishlistId = wishlistId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.wishListOwnerName = wishListOwnerName;
        this.hasOffer = hasOffer;
        this.zipCode = zipCode;
    }


}