export class WishlistPageOfferMatchRequestInfo {
    offerOwnerId: number;
    wishListId: number;
    startTime: Date;
    endTime: Date;
    zipCode: number;
    hostName: string;

    constructor(offerOwnerId =-1,
    wishListId = -1,
    startTime = new Date(),
    endTime = new Date(),
    zipCode = -1,
    hostName = ""
    ) {
        this.offerOwnerId = offerOwnerId;
        this.wishListId = wishListId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.zipCode = zipCode;
        this.hostName = hostName;
    }


}