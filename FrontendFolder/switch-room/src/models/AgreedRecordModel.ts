export class AgreedRecordModel {
    public readonly id: number;
    public stateCityCode: number;
    public state: string;
    public city:string;
    public wishlistId: number;
    public wishlistUserId: number;
    public offerId: number;
    public startTime: Date|any;
    public endTime: Date|any;
    public toOfferStar: number;
    public toVisitorStar: number;
    public toOfferComment: string;
    public toVisitorComment: string;
    public onGoing: boolean|any;
    constructor(
        id = -1,
        stateCityCode = -1,
        state = "",
        city = "",
        wishlistId = -1,
        wishlistUserId = -1,
        offerId = -1,
        startTime = null,
        endTime = null,
        toOfferStar = 5,
        toVisitorStar = 5,
        toOfferComment = "",
        toVisitorComment = "",
        onGoing = null,
    ){
        this.id = id;
        this.stateCityCode = stateCityCode;
        this.state = state;
        this.city = city;
        this.wishlistId = wishlistId;
        this.wishlistUserId = wishlistUserId;
        this.offerId = offerId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.toOfferStar = toOfferStar;
        this.toVisitorStar = toVisitorStar;
        this.toOfferComment = toOfferComment;
        this.toVisitorComment = toVisitorComment;
        this.onGoing = onGoing;
    }
}