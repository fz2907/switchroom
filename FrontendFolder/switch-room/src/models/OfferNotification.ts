export class OfferNotification {
  public offerId: number;
  public wishlistItemId: number;
  public modifyDate: Date;
  public offerResult: number;
  public wishlistResult: number;

  constructor(
    offerId = -1,
    wishlistItemId = -1,
    modifyDate = new Date(),
    offerResult = -1,
    wishlistResult = -1
  ) {
    this.offerId = offerId;
    this.wishlistItemId = wishlistItemId;
    this.modifyDate = modifyDate;
    this.offerResult = offerResult;
    this.wishlistResult = wishlistResult;
  }
}
