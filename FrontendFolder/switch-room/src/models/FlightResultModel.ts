export class FlightResultModel {
    public id : number;
    public departure0: string;
    public arrival0: string;
    public departureDateTime_0: string;
    public arrivalDateTime_0: string;
    public classOfService_0: string;
    public aircraftType_0: string;
    public distance_0: number;

    public departure1: string;
    public arrival1: string;
    public departureDateTime_1: string;
    public arrivalDateTime_1: string;
    public classOfService_1: string;
    public aircraftType_1: string;
    public distance_1: number;

    public price: number;
    public airlines: string;
    public detail_url: string;


    constructor(id: number, departure0: string, arrival0: string, departureDateTime_0: string, arrivalDateTime_0: string, classOfService_0: string, aircraftType_0: string, distance_0: number, departure1: string, arrival1: string, departureDateTime_1: string, arrivalDateTime_1: string, classOfService_1: string, aircraftType_1: string, distance_1: number, price: number, airlines: string, detail_url: string) {
        this.id = id;
        this.departure0 = departure0;
        this.arrival0 = arrival0;
        this.departureDateTime_0 = departureDateTime_0;
        this.arrivalDateTime_0 = arrivalDateTime_0;
        this.classOfService_0 = classOfService_0;
        this.aircraftType_0 = aircraftType_0;
        this.distance_0 = distance_0;
        this.departure1 = departure1;
        this.arrival1 = arrival1;
        this.departureDateTime_1 = departureDateTime_1;
        this.arrivalDateTime_1 = arrivalDateTime_1;
        this.classOfService_1 = classOfService_1;
        this.aircraftType_1 = aircraftType_1;
        this.distance_1 = distance_1;
        this.price = price;
        this.airlines = airlines;
        this.detail_url = detail_url;
    }
}
