export class InteractionModel {
  public userId1: number;
  public userId2: number;
  public phone1: string;
  public phone2: string;
  public judgement: boolean;
  public offerId: string;
  public wishlistId: string;

  constructor(
    userId1 = -1,
    userId2 = -1,
    phone1 = "",
    phone2 = "",
    judgement = false,
    offerId = "",
    wishlistId = ""
  ) {
    this.userId1 = userId1;
    this.userId2 = userId2;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.judgement = judgement;
    this.offerId = offerId;
    this.wishlistId = wishlistId;
  }
}
