export class WishlistItemModel {
  public readonly wishlistItemId: number;
  public cityName: string;
  public stateCityCode: number;
  public zipCode: number| string
  public state: string;
  public stateCode: number;
  public startTime: Date | string;
  public endTime: Date | string;
  public details: string;

  constructor(
    wishlistItemId = -1,
    cityName = "",
    state = "",
    stateCityCode = -1,
    stateCode = -1,
    startTime = "",
    endTime = "",
    details = "",
    zipCode=""
  ) {
    this.wishlistItemId = wishlistItemId;
    this.cityName = cityName;
    this.state = state;
    this.stateCode = stateCode;
    this.stateCityCode = stateCityCode;
    this.startTime = startTime;
    this.endTime = endTime;
    this.details = details;
    this.zipCode = zipCode;
  }
}
