export class OfferFormModel {
  public readonly userId: number;
  public state: string;
  public stateCode: number;
  public zipCode: number | string;
  public spaceType: string;
  public otherSpaceType: string;
  public spaceLocateCity: string;
  public stateCityCode: number;
  public availableTimeStart: Date | string;
  public availableTimeEnd: Date | string;
  public maxNumberOfPeople: number;
  public spaceDetails: string;
  public offering: boolean;

  constructor(
    userId = -1,
    state = "",
    stateCode = -1,
    zipCode = "",
    spaceType = "",
    otherSpaceType = "",
    spaceLocateCity = "",
    stateCityCode = -1,
    offering = false,
    availableTimeStart = new Date(),
    availableTimeEnd = new Date(),
    maxNumberOfPeople = 0,
    spaceDetails = ""
  ) {
    this.userId = userId;
    this.state = state;
    this.stateCode = stateCode;
    this.zipCode = zipCode;
    this.spaceType = spaceType;
    this.otherSpaceType = otherSpaceType;
    this.spaceLocateCity = spaceLocateCity;
    this.stateCityCode = stateCityCode;
    this.offering = offering;
    this.availableTimeStart = availableTimeStart;
    this.availableTimeEnd = availableTimeEnd;
    this.maxNumberOfPeople = maxNumberOfPeople;
    this.spaceDetails = spaceDetails;
  }
}
