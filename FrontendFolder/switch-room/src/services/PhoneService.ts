import * as serverHttpService from "./ServerHttpService";

const baseUrl = "interactive";
function postPhoneToServer(phoneNumber: any) {
  const urlPath = "/newData";
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(phoneNumber));
}

function fetchFromServer(userId: any) {
  const urlPath = "/" + userId;
  return serverHttpService.Get(baseUrl + urlPath);
}

export { postPhoneToServer, fetchFromServer };
