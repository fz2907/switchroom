/**
 * This is a class that contains all constant values, it help to make change on address or ports or other things.
 */

const SEVER_IP = location.hostname;

const SERVER_PORT = 8081;
const Server_URL =
  location.protocol + "//" + SEVER_IP + ":" + SERVER_PORT + "/";

const USA_STATE_INITAL_LIST = [
  "AL",
  "AK",
  "AS",
  "AZ",
  "AR",
  "CA",
  "CO",
  "CT",
  "DE",
  "DC",
  "FL",
  "GA",
  "GU",
  "HI",
  "ID",
  "IL",
  "IN",
  "IA",
  "KS",
  "KY",
  "LA",
  "ME",
  "MD",
  "MA",
  "MI",
  "MN",
  "MS",
  "MO",
  "MT",
  "NE",
  "NV",
  "NH",
  "NJ",
  "NM",
  "NY",
  "NC",
  "ND",
  "MP",
  "OH",
  "OK",
  "OR",
  "PA",
  "PR",
  "RI",
  "SC",
  "SD",
  "TN",
  "TX",
  "UT",
  "VT",
  "VA",
  "VI",
  "WA",
  "WV",
  "WI",
  "WY",
];

export { SEVER_IP, SERVER_PORT, Server_URL, USA_STATE_INITAL_LIST };

/*
 Change logs:
 Date        |       Author          |   Description
 2022-10-12  |    Fangzheng Zhang    |    create class and init
 2022-10-17  |    Fangzheng Zhang    |      Change to TS 
 
  */
