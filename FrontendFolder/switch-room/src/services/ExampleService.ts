/**
 * This is an example service class to show you that how to use http service in ServerHttpService class.
 * If you have some data pre-processing job, you can do it here.
 */

import * as serverHttpService from "./ServerHttpService";
import { Server_URL } from "@/services/Constans";

const baseUrl = "example";

function getExapleFromServer() {
  return serverHttpService.Get(baseUrl);
}

function getExampleWithIDFromServer(inputId: number | string) {
  const urlPath = "/" + inputId;
  return serverHttpService.Get(baseUrl + urlPath);
}

function getExampleUserFromServer() {
  const urlPath = "/getUser";
  return serverHttpService.Get(baseUrl + urlPath);
}

function postExampleUserDataToServer(userData: any) {
  const urlPath = "/newUser";
  // console.log(Server_URL + baseUrl + urlPath);
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(userData));
}

export {
  getExapleFromServer,
  getExampleWithIDFromServer,
  getExampleUserFromServer,
  postExampleUserDataToServer,
};
