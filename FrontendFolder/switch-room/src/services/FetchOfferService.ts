import * as serverHttpService from "./ServerHttpService";

const baseUrl = "matchedOffer";

function getOffersFromServerWithID(offerId: number) {
  const urlPath = "/" + offerId;
  return serverHttpService.Get(baseUrl + urlPath);
}

export { getOffersFromServerWithID };
