import * as serverHttpService from "@/services/ServerHttpService";

const baseUrl = "user";
function postUserDataToServer(userData: any) {
  const urlPath = "/newUser";
  // console.log(Server_URL + baseUrl + urlPath);
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(userData));
}
function loginUser(userData: any) {
  const urlPath = "/loginUser";
  // console.log(Server_URL + baseUrl + urlPath);
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(userData));
}
function checkLoginSession() {
  const urlPath = "/checkLoginSession";
  return serverHttpService.Get(baseUrl + urlPath);
}
function resetPassword(userData: any) {
  const urlPath = "/resetPassword";
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(userData));
}
function forgotPassword(userData: any) {
  const urlPath = "/forgotPassword";
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(userData));
}
function forgotPasswordCreateNew(userData: any) {
  const urlPath = "/forgotPasswordCreateNew";
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(userData));
}
function getProfile() {
  const urlPath = "/profile";
  return serverHttpService.Get(baseUrl + urlPath);
}

function getSpecificProfile(userId: any) {
  const urlPath = "/" + userId;
  return serverHttpService.Get(baseUrl + urlPath);
}

export {
  postUserDataToServer,
  loginUser,
  checkLoginSession,
  resetPassword,
  forgotPassword,
  forgotPasswordCreateNew,
  getProfile,
  getSpecificProfile,
};
