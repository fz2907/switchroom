/**
 * This class will fetch and send user wishlist data from back end
 */

import * as serverHttpService from "./ServerHttpService";
import { WishlistItemModel } from "@/models/WishlistItemModel";

const baseUrl = "wishlist";

/**
 * This function will fetch users wishlist item list
 */
export function getUserWishlistInfo() {
  return serverHttpService.Get(baseUrl);
}

/**
 * Send new wishlistItem information to server to create new record
 * @param wishlistItem  new wishlistItem
 */
export function createNewWishlistItem( wishlistItem: WishlistItemModel ) {
  return serverHttpService.Post(baseUrl + "/newWishlistItem", wishlistItem);
}

/**
 * Update an sexist wishlistItem
 * @param userId user's id
 * @param wishlistItem changed wishlistItem
 */
export function updateWishlistItem(wishlistItem: WishlistItemModel) {
  return serverHttpService.Post(baseUrl + "/updateWishlistItem", wishlistItem);
}

/**
 * Remove one of user's wishlistItem from DB
 * @param userId user's id
 * @param wishlistItemId wishlist Item's Id
 */
export function deleteWishlistItem(wishlistItemId: number) {
  const urlPath =
    baseUrl +
    "/deleteWishlistItem/" +
    wishlistItemId;
  return serverHttpService.Delete(urlPath);
}

export function getOfferMatchList(wishlistItemId: number){
  const urlPath = baseUrl + "/loadOfferMatchList/"+wishlistItemId;
  return serverHttpService.Get(urlPath);
}

export function loadMatchOfferNumbersFor(wishlistItemList: Array<WishlistItemModel>){
  let wishlistIdList : Array<number>= [];
  wishlistItemList.forEach((wishlistItem) => {
    wishlistIdList.push(wishlistItem.wishlistItemId);
  })
  const urlPath = baseUrl + "/loadOfferMatchCount";
  return serverHttpService.Post(urlPath, wishlistIdList);
}

/*
Change logs:
Date        |       Author          |   Description
2022-10-20  |    Fangzheng Zhang    |    create class and init
2022-11-05  |    Fangzheng Zhang    |   Add loadMatchOfferNumbersFor and getOfferMatchList to fetch match data
 */
