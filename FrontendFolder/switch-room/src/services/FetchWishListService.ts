import * as serverHttpService from "./ServerHttpService";

const baseUrl = "matchedWishList";

function getWishListFromServerWithID(wishListId: number) {
  const urlPath = "/" + wishListId;
  return serverHttpService.Get(baseUrl + urlPath);
}

export { getWishListFromServerWithID };
