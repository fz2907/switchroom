/**
 * This class will fetch and send user offer data from back end
 */

import * as serverHttpService from "./ServerHttpService";
import { FlightFormModel } from "../models/FlightFormModel";

const baseUrl = "searchflight";

/**
 * Submit a new offer form
 * @param flightForm the offer from object
 */
export function createNewFlight(flightForm: FlightFormModel) {
  return serverHttpService.Post(baseUrl + "/newFlight", flightForm);
}
