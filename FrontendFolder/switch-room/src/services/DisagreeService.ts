import * as serverHttpService from "./ServerHttpService";

const baseUrl = "interactive";
function postDisagreeToServer(data: any) {
  const urlPath = "/disagree";
  return serverHttpService.Post(baseUrl + urlPath, JSON.parse(data));
}

// function fetchFromServer(userId: any) {
//     const urlPath = "/" + userId;
//     return serverHttpService.Get(baseUrl + urlPath);
// }

export { postDisagreeToServer };
