import * as serverHttpService from "./ServerHttpService";

const baseUrl = "notification";

function getNotificationFromServerWithID(userId: number) {
  const urlPath = "/" + userId;
  return serverHttpService.Get(baseUrl + urlPath);
}

function getNotificationwishFromServerWithID(userId: number) {
  const urlPath = "/wish/" + userId;
  return serverHttpService.Get(baseUrl + urlPath);
}

export { getNotificationFromServerWithID, getNotificationwishFromServerWithID };
