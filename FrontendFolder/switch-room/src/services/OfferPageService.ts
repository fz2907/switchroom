/**
 * This class will fetch and send user offer data from back end
 */

import * as serverHttpService from "./ServerHttpService";
import { OfferFormModel } from "../models/OfferFormModel";

const baseUrl = "offer";

/**
 * Get offer with userID
 * @param userId the current user's id
 */
export async function getUserOfferInfoFromServer() {
  return serverHttpService.Get(baseUrl);
}

/**
 * Submit a new offer form
 * @param userId user's id
 * @param offerForm the offer from object
 */
export function createNewUserOffer(offerForm: OfferFormModel) {
  return serverHttpService.Post(baseUrl + "/newOffer", offerForm);
}

/**
 * update offer
 * @param userId user's id
 * @param offerForm new form
 */
export function updateOffer(offerForm: OfferFormModel) {
  return serverHttpService.Post(baseUrl + "/updateOffer", offerForm);
}

/**
 * Delete an exists form
 * @param userId user's id
 * @param offerId form id
 */
export function deleteOffer(userId : number) {
  const urlPath = baseUrl + "/deleteOffer/"+userId;
  return serverHttpService.Delete(urlPath);
}

export function getWishlistMatchRequestInfoList(){
  const urlPath = baseUrl + "/wishlistMatchRequestInfoList"
  return serverHttpService.Get(urlPath);
}

/*
Change logs:
Date        |       Author          |   Description
2022-10-20  |    Fangzheng Zhang    |    create class and init
2022-11-03  |    Fangzheng Zhang    |    Remove user Id in parameter because cookie is ready
 */
