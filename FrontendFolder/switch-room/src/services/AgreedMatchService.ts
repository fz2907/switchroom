import * as serverHttpService from "./ServerHttpService";
import {AgreedRecordModel} from "@/models/AgreedRecordModel";

const baseUrl = "agreedRecord";

export function getAgreedOfferRecord() {
    const urlPath = "/offer";
    return serverHttpService.Get(baseUrl + urlPath);
}
export function getAgreedWishlistRecord() {
    const urlPath = "/wishlist";
    return serverHttpService.Get(baseUrl + urlPath);
}
export function updateRatingInformation(agreedRecord: AgreedRecordModel) {
    return serverHttpService.Post(baseUrl + "/updateCommentItem", agreedRecord);
}