export interface User {
  user_id: number;
  username: string;
  password: string;
  email: string;
  firstname: string;
  lastname: string;
  gender: string;
}
