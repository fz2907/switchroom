import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/HomeView.vue";
import RegisterView from "../views/RegisterView.vue";
import ForgotPasswordView from "../views/ForgotPasswordView.vue";
import ForgotPasswordFormView from "../views/ForgotPasswordFormView.vue";
import ResetPasswordView from "../views/ResetPasswordView.vue";
import ProfileView from "../views/ProfileView.vue";
import MatchedView from "../views/MatchedView.vue";
import AgreementView from "../views/AgreementView.vue";
import SendView from "../views/SendView.vue";
import ConfirmationView from "../views/ConfirmationView.vue";
import ResultsView from "../views/ResultsView.vue";
import ConfirmationFailView from "../views/ConfirmationFailView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
    meta: {
      hideHeader: true,
    },
  },
  {
    path: "/register",
    name: "register",
    component: RegisterView,
    meta: {
      hideHeader: true,
    },
  },
  {
    path: "/forgotPassword",
    name: "forgotPassword",
    meta: {
      hideHeader: true,
    },
    component: ForgotPasswordView,
  },
  {
    path: "/forgotPasswordForm",
    name: "forgotPasswordForm",
    meta: {
      requiresAuth: true,
      hideHeader: true,
    },
    component: ForgotPasswordFormView,
  },
  {
    path: "/resetPassword",
    name: "resetPassword",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: ResetPasswordView,
  },
  {
    path: "/offer-page",
    name: "OfferPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: () =>
      import(/* webpackChunkName:  "about" */ "../views/OfferView.vue"),
  },
  {
    path: "/wishlist-page",
    name: "WishlistPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/WishlistView.vue"),
  },
  {
    path: "/agreed-match-page",
    name: "AgreedMatchPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: () =>
        import(/* webpackChunkName: "about" */ "../views/AgreedMatchView.vue"),
  },
  {
    path: "/flight",
    name: "flight",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/FlightView.vue"),
  },
  {
    path: "/profile",
    name: "profile",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: ProfileView,
  },
  {
    path: "/login-main-page",
    name: "LoginMainPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: () => import("../views/LoginMainPageView.vue"),
  },

  {
    path: "/matched-result/:offerId/:wishlistId",
    name: "MatchResultPage",
    component: MatchedView,
  },
  {
    path: "/flight-ticket",
    name: "TicketPage",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/FlightTicket.vue"),
  },
  {
    path: "/agree/:offerId/:wishlistId",
    name: "AgreementPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: AgreementView,
  },
  {
    path: "/agree/send/:offerId/:wishlistId",
    name: "sendPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: SendView,
  },
  {
    path: "/confirmation",
    name: "confirmationPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: ConfirmationView,
  },
  {
    path: "/confirmationFail",
    name: "confirmationFailPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: ConfirmationFailView,
  },

  {
    path: "/results",
    name: "resultsPage",
    meta: {
      requiresAuth: true,
      hideHeader: false,
    },
    component: ResultsView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!document.cookie) {
      next({ name: "home" });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
