import { createApp } from "vue";
import ElementPlus from "element-plus";
import store from "./store/index";
import * as cookieParser from "cookie-parser";
import "element-plus/dist/index.css";
import App from "./App.vue";
import router from "./router";
/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";

/* import font awesome icon component */
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

/* import specific icons */
import { faUserSecret } from "@fortawesome/free-solid-svg-icons";

import { fas } from "@fortawesome/free-solid-svg-icons";

import { faFacebook } from "@fortawesome/free-brands-svg-icons";

import { faTwitter } from "@fortawesome/free-brands-svg-icons";

import { faLinkedin } from "@fortawesome/free-brands-svg-icons";

import { faGithub } from "@fortawesome/free-brands-svg-icons";



import {createPinia} from "pinia";

/* add icons to the library */
library.add(faUserSecret);
library.add(fas);
library.add(faFacebook);
library.add(faTwitter);
library.add(faLinkedin);
library.add(faGithub);
const pinia = createPinia();


createApp(App)
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(router)
  .use(ElementPlus)
  .use(store)
    .use(pinia)
  .mount("#app");
