package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.WishlistItemModel;

import java.util.Date;
import java.util.List;

public interface WishlistItemRepository extends JpaRepository<WishlistItemModel, Long> {
    WishlistItemModel findByWishlistItemId(Long wishlistItemId);
    void deleteByWishlistItemId(Long wishlistItemId);
    List<WishlistItemModel> findAllByModifyDateAfter(Date modifyDate);

}
