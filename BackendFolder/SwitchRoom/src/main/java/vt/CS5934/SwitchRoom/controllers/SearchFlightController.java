/**
 * This class with handle all the http request start with 'searchflight'
 */
package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.FlightResultModel;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.models.SerachFlightModel;
import vt.CS5934.SwitchRoom.services.FlightApiService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("searchflight")
public class SearchFlightController {

    private final Logger logger = LoggerFactory.getLogger(SearchFlightController.class);

    @Autowired
    FlightApiService flightApiService;

    @PostMapping("/newFlight")
    public ResponseModel createNewSearchFlight(@RequestBody SerachFlightModel newFlight){
        ResponseModel responseModel = new ResponseModel();
        try{
            List<FlightResultModel> flightList = flightApiService.performSearch(newFlight);
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(flightList);
//            System.out.println(flightApiService.performSearch(newFlight));
//            responseModel.setData(newFlight);
            return responseModel;
        }catch (Exception e){
            logger.error("Error in createSearchFlight: "+e);
            responseModel.setMessage("Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
//        System.out.println(newFlight.getDestination());
//        return newFlight.getDestination();
    }


}
