package vt.CS5934.SwitchRoom.models;

public class InteractionCombinedModel {

    private InteractionModel[] list1;

    private InteractionModel[] list2;

    private UserModel[] user1;

    private UserModel[] user2;

    public InteractionCombinedModel(InteractionModel[] list1, InteractionModel[] list2, UserModel[] user1, UserModel[] user2) {
        this.list1 = list1;
        this.list2 = list2;
        this.user1 = user1;
        this.user2 = user2;
    }

    public InteractionModel[] getList1() {
        return list1;
    }

    public void setList1(InteractionModel[] list1) {
        this.list1 = list1;
    }

    public InteractionModel[] getList2() {
        return list2;
    }

    public void setList2(InteractionModel[] list2) {
        this.list2 = list2;
    }

    public UserModel[] getUser1() {
        return user1;
    }

    public void setUser1(UserModel[] user1) {
        this.user1 = user1;
    }

    public UserModel[] getUser2() {
        return user2;
    }

    public void setUser2(UserModel[] user2) {
        this.user2 = user2;
    }
}
