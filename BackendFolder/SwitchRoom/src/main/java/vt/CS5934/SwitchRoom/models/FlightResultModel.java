package vt.CS5934.SwitchRoom.models;

public class FlightResultModel {

    private Integer id;
    //    Travel detail
    private String departure0;
    private String arrival0;
    private String departureDateTime_0;
    private String arrivalDateTime_0;
    private String classOfService_0;
    private String aircraftType_0;
    private Double distance_0;


    //    Return detail
    private String departure_1;
    private String arrival_1;
    private String departureDateTime_1;
    private String arrivalDateTime_1;
    private String classOfService_1;
    private String aircraftType_1;
    private Double distance_1;
    private Double price;
    private String airlines;
    private String detail_url;


    public FlightResultModel(Integer id, String departure0, String arrival0, String departureDateTime_0, String arrivalDateTime_0, String classOfService_0, String aircraftType_0, Double distance_0, String departure_1, String arrival_1, String departureDateTime_1, String arrivalDateTime_1, String classOfService_1, String aircraftType_1, Double distance_1, Double price, String airlines, String detail_url) {
        this.id = id;
        this.departure0 = departure0;
        this.arrival0 = arrival0;
        this.departureDateTime_0 = departureDateTime_0;
        this.arrivalDateTime_0 = arrivalDateTime_0;
        this.classOfService_0 = classOfService_0;
        this.aircraftType_0 = aircraftType_0;
        this.distance_0 = distance_0;
        this.departure_1 = departure_1;
        this.arrival_1 = arrival_1;
        this.departureDateTime_1 = departureDateTime_1;
        this.arrivalDateTime_1 = arrivalDateTime_1;
        this.classOfService_1 = classOfService_1;
        this.aircraftType_1 = aircraftType_1;
        this.distance_1 = distance_1;
        this.price = price;
        this.airlines = airlines;
        this.detail_url = detail_url;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeparture0() {
        return departure0;
    }

    public void setDeparture0(String departure0) {
        this.departure0 = departure0;
    }

    public String getArrival0() {
        return arrival0;
    }

    public void setArrival0(String arrival0) {
        this.arrival0 = arrival0;
    }

    public String getDepartureDateTime_0() {
        return departureDateTime_0;
    }

    public void setDepartureDateTime_0(String departureDateTime_0) {
        this.departureDateTime_0 = departureDateTime_0;
    }

    public String getArrivalDateTime_0() {
        return arrivalDateTime_0;
    }

    public void setArrivalDateTime_0(String arrivalDateTime_0) {
        this.arrivalDateTime_0 = arrivalDateTime_0;
    }

    public String getClassOfService_0() {
        return classOfService_0;
    }

    public void setClassOfService_0(String classOfService_0) {
        this.classOfService_0 = classOfService_0;
    }

    public String getAircraftType_0() {
        return aircraftType_0;
    }

    public void setAircraftType_0(String aircraftType_0) {
        this.aircraftType_0 = aircraftType_0;
    }

    public Double getDistance_0() {
        return distance_0;
    }

    public void setDistance_0(Double distance_0) {
        this.distance_0 = distance_0;
    }

    public String getDeparture_1() {
        return departure_1;
    }

    public void setDeparture_1(String departure_1) {
        this.departure_1 = departure_1;
    }

    public String getArrival_1() {
        return arrival_1;
    }

    public void setArrival_1(String arrival_1) {
        this.arrival_1 = arrival_1;
    }

    public String getDepartureDateTime_1() {
        return departureDateTime_1;
    }

    public void setDepartureDateTime_1(String departureDateTime_1) {
        this.departureDateTime_1 = departureDateTime_1;
    }

    public String getArrivalDateTime_1() {
        return arrivalDateTime_1;
    }

    public void setArrivalDateTime_1(String arrivalDateTime_1) {
        this.arrivalDateTime_1 = arrivalDateTime_1;
    }

    public String getClassOfService_1() {
        return classOfService_1;
    }

    public void setClassOfService_1(String classOfService_1) {
        this.classOfService_1 = classOfService_1;
    }

    public String getAircraftType_1() {
        return aircraftType_1;
    }

    public void setAircraftType_1(String aircraftType_1) {
        this.aircraftType_1 = aircraftType_1;
    }

    public Double getDistance_1() {
        return distance_1;
    }

    public void setDistance_1(Double distance_1) {
        this.distance_1 = distance_1;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAirlines() {
        return airlines;
    }

    public void setAirlines(String airlines) {
        this.airlines = airlines;
    }

    public String getDetail_url() {
        return detail_url;
    }

    public void setDetail_url(String detail_url) {
        this.detail_url = detail_url;
    }
}
