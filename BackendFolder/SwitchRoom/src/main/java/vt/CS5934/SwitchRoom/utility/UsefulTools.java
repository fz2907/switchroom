package vt.CS5934.SwitchRoom.utility;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;

public class UsefulTools {
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public static String getSiteURL(HttpServletRequest request) {
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }
}
