package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import vt.CS5934.SwitchRoom.models.UserModel;

import java.util.List;


/**
 * The interface have to extend JpaRepository, and the type of the class should be
 * < The_model_class, Integer>. In this example, the model class is ExampleModel
 */
public interface UserRepository extends JpaRepository<UserModel, Integer> {

    /**
     * The function name is the SQL query:
     *  findByIdAndName(long inputId, String inputName) == "SELETE * FROM table WHERE Id==inputId" AND name == inputName;
     * This is an example of how to declare a SQL command, those will be use in service class
     * @param userId the id in table you are looking for
     * @return ExampleModel object
     */
    UserModel findByUserId(int userId);

    UserModel findByUsername(String username);

    UserModel findByEmail(String email);

    UserModel findByResetPasswordToken(String token);

    List<UserModel> findAll();
    void deleteByUserId(Long userId);

}
