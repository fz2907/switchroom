/**
 * This class with handle all the http request start with 'offer'
 */
package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.models.UserOfferModel;
import vt.CS5934.SwitchRoom.models.WishlistMatchRequestInfo;
import vt.CS5934.SwitchRoom.services.OfferPageService;

import java.util.ArrayList;

@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("offer")
public class OfferPageController {

    private final Logger logger = LoggerFactory.getLogger(OfferPageController.class);

    @Autowired
    OfferPageService offerPageService;
    /**
     * This class will fetch user's offer from DB and send to front end
     * @return ResponseModel contains offer data
     */
    @GetMapping
    public ResponseModel getUserOfferInfo(@CookieValue(value = "userId") Long userId){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(offerPageService.getUserOfferInfo(userId));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getUserOfferInfo: "+e);
            responseModel.setMessage("Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @PostMapping("/newOffer")
    public ResponseModel createNewUserOffer(@RequestBody UserOfferModel offerModel){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(offerPageService.saveNewUserOffer(offerModel));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in createNewUserOffer: "+e.fillInStackTrace());
            responseModel.setMessage("Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @PostMapping("/updateOffer")
    public ResponseModel updateUserOffer(@RequestBody UserOfferModel offerModel){
        offerPageService.removeOfferFromMatchedTable(offerModel.getUserId());
        return createNewUserOffer(offerModel);
    }

    @DeleteMapping("/deleteOffer/{userId}")
    public ResponseModel deleteUserOffer(@PathVariable Long userId){
        ResponseModel responseModel = new ResponseModel();
        try{
            offerPageService.deleteUserOffer(userId);
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(true);
            return responseModel;
        }catch (Exception e){
            logger.error("Error in deleteUserOffer: "+e);
            responseModel.setMessage("Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(false);
            return responseModel;
        }
    }

    @GetMapping("/wishlistMatchRequestInfoList")
    public ResponseModel getWishlistMatchRequestInfoList(@CookieValue(value = "userId") Long userId){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(offerPageService.getWishlistMatchRequestInfoList(userId));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getWishlistMatchRequestInfoList: "+e.fillInStackTrace());
            responseModel.setMessage("Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(new ArrayList<WishlistMatchRequestInfo>());
            return responseModel;
        }
    }

}
/*
Change logs:
Date        |       Author          |   Description
2022-11-05  |    Fangzheng Zhang    |   add getWishlistMatchRequestInfoList function
 */
