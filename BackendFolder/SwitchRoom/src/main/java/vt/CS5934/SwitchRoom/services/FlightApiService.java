package vt.CS5934.SwitchRoom.services;

import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.models.FlightResultModel;
import vt.CS5934.SwitchRoom.models.SerachFlightModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.Serializable;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
@Service
public class FlightApiService implements Serializable {
    private List<FlightResultModel> listOfFlightResults;

    public List<FlightResultModel> getListOfSearchedFlights() {
        return listOfFlightResults;
    }

    public void setListOfSearchedFlights(List<FlightResultModel> listOfFlightResults) {
        this.listOfFlightResults = listOfFlightResults;
    }
    private final Logger logger = LoggerFactory.getLogger(FlightApiService.class);
    public List<FlightResultModel> performSearch(SerachFlightModel newFlight) {

        listOfFlightResults = new ArrayList<>();

        try {
//            String URI = "https://tripadvisor16.p.rapidapi.com/api/v1/flights/searchFlights?" +
//                    "sourceAirportCode=" + upperCase(newFlight.getDeparture()) + "&destinationAirportCode="
//                    + upperCase(newFlight.getDestination()) + "&date=" + convertDate1(newFlight.getDate1()) + "&itineraryType=ROUND_TRIP&sortOrder=PRICE&numAdults=" + newFlight.getNumAdults() + "&numSeniors=" + newFlight.getNumSeniors() + "&classOfService=" + newFlight.getClassOfService() + "&returnDate=" + convertDate1(newFlight.getDate2()) + "&currencyCode=USD";
//            System.out.println(URI);
//            HttpRequest request = HttpRequest.newBuilder()
//                    .uri(java.net.URI.create(URI))
//                    .header("X-RapidAPI-Key", "ccf2ddc79amsh1e381193b5f63a9p179bfdjsn9ed1d6b269f7")
//                    .header("X-RapidAPI-Host", "tripadvisor16.p.rapidapi.com")
//                    .method("GET", HttpRequest.BodyPublishers.noBody())
//                    .build();
//            HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
//            JSONObject searchResultsJsonObject = new JSONObject(response.body());

            String file = "flightapi.json";
            String json = readFileAsString(file);
            JSONObject searchResultsJsonObject = new JSONObject(json);

            JSONObject dataJsonObject = searchResultsJsonObject.getJSONObject("data");
            JSONArray flightJsonArray = dataJsonObject.getJSONArray("flights");


            int index = 0;
            if (flightJsonArray.length() > index) {
                while (flightJsonArray.length() > index) {
                    JSONObject flightObject = flightJsonArray.getJSONObject(index);

                    JSONArray segmentsJSONArray = flightObject.getJSONArray("segments");

                    //    ************* travel **************
                    JSONObject firstObject = segmentsJSONArray.getJSONObject(0);
                    JSONArray legsObject = firstObject.getJSONArray("legs");
                    JSONObject TravelObject = legsObject.getJSONObject(0);

                    JSONObject secondObject = segmentsJSONArray.getJSONObject(1);
                    JSONArray legsObj = secondObject.getJSONArray("legs");
                    JSONObject ReturnObject = legsObj.getJSONObject(0);

                    JSONArray purchaseLinksJSONArray = flightObject.getJSONArray("purchaseLinks");
                    JSONObject purchaseObject = purchaseLinksJSONArray.getJSONObject(0);

                    String departure_0 = TravelObject.optString("originStationCode", "");
                    if (departure_0.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String arrival_0 = TravelObject.optString("destinationStationCode", "");
                    if (arrival_0.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String departureDateTime_0 = TravelObject.optString("departureDateTime", "");
                    if (departureDateTime_0.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String arrivalDateTime_0 = TravelObject.optString("arrivalDateTime", "");
                    if (arrivalDateTime_0.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }


                    String classOfService_0 = TravelObject.optString("classOfService", "");
                    if (classOfService_0.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String aircraftType_0 = TravelObject.optString("equipmentId", "");
                    if (aircraftType_0.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    Double distance_0 = TravelObject.optDouble("distanceInKM", 0.0);
                    if (distance_0 == 0.0) {
                        index++;
                        continue;   // Jump to the next iteration
                    }else {
                        /* Round the calories value to 2 decimal places */
                        distance_0 = distance_0 * 100;
                        distance_0 = (double) Math.round(distance_0);
                        distance_0 = distance_0 / 100;
                    }

                    //    ************* return **************
//                    JSONObject secondObject = segmentsJSONArray.getJSONObject(1);
//                    JSONArray legsObj = secondObject.getJSONArray("legs");
//                    JSONObject ReturnObject = legsObj.getJSONObject(0);


                    String departure_1 = ReturnObject.optString("originStationCode", "");
                    if (departure_1.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String arrival_1 = ReturnObject.optString("destinationStationCode", "");
                    if (arrival_1.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String departureDateTime_1 = ReturnObject.optString("departureDateTime", "");
                    if (departureDateTime_1.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String arrivalDateTime_1 = ReturnObject.optString("arrivalDateTime", "");
                    if (arrivalDateTime_1.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }


                    String classOfService_1 = ReturnObject.optString("classOfService", "");
                    if (classOfService_1.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    String aircraftType_1 = ReturnObject.optString("equipmentId", "");
                    if (aircraftType_1.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }


                    Double distance_1 = ReturnObject.optDouble("distanceInKM", 0.0);
                    if (distance_1 == 0.0) {
                        index++;
                        continue;   // Jump to the next iteration
                    }else {
                        /* Round the calories value to 2 decimal places */
                        distance_1 = distance_1 * 100;
                        distance_1 = (double) Math.round(distance_1);
                        distance_1 = distance_1 / 100;
                    }

//                    JSONArray purchaseLinksJSONArray = flightObject.getJSONArray("purchaseLinks");
//                    JSONObject purchaseObject = purchaseLinksJSONArray.getJSONObject(0);

                    String airline = purchaseObject.optString("providerId", "");
                    if (airline.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    Double price = purchaseObject.optDouble("totalPrice", 0.0);
                    if (price == 0.0) {
                        index++;
                        continue;   // Jump to the next iteration

                    }else {
                        /* Round the calories value to 2 decimal places */
                        price = price * 100;
                        price = (double) Math.round(price);
                        price = price / 100;
                    }

                    String url = purchaseObject.optString("url", "");
                    if (url.equals("")) {
                        index++;
                        continue;   // Jump to the next iteration
                    }

                    FlightResultModel flight = new FlightResultModel(index,departure_0,arrival_0,convertDate(departureDateTime_0),convertDate(arrivalDateTime_0),classOfService_0,aircraftType_0,distance_0,departure_1,arrival_1,convertDate(departureDateTime_1),convertDate(arrivalDateTime_1),classOfService_1,aircraftType_1,distance_1,price,airline,url);
//                    System.out.println(flight);
                    listOfFlightResults.add(flight);
                    index++;

                }
            }
            else {
                logger.error("Error in getFlightApiService: ");
            }
        }catch (Exception ex){
            logger.error("Error in getFlightApiService: "+ex);
        }
        return listOfFlightResults;
    }
    public static String upperCase(String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase();
    }
    public static String convertDate(String d) {
        try {
            //2022-12-01T06:30:00+05:30
            SimpleDateFormat oldDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            Date date = oldDateFormat.parse(d);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mmaaa");
            return simpleDateFormat.format(date);
        }
        catch (ParseException exc) {
            return "N/A";
        }
    }

    public static String convertDate1(Date d) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(d);
    }
    public static String readFileAsString(String file)throws Exception
    {
        return new String(Files.readAllBytes(Paths.get(file)));
    }
}

