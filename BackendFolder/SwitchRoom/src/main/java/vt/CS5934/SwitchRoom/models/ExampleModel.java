/**
 * This class this a data model example.
 * Model mains data class, it may or may not be saved to DB
 */

package vt.CS5934.SwitchRoom.models;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * '@Data' tell spring this class can be map to JSON object if needed
 * '@Entity' declare this class is a DB table
 * '@Table(name=<table_name>)' declared which table stores its data
 */
@Data
@Entity
@Table(name = "example_records_table")
@NoArgsConstructor
public class ExampleModel {
    /**
     * '@Id' declare that userId object is the primary id in this table
     * '@Column' you can set the table column name in the DB
     */
    @Id
    @Column(name="user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    @Column(name="username",unique=true)
    private String username;

    public ExampleModel(String name) {
        this.username = name;
    }

    @Override
    public String toString() {
        return "ExampleModel{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                '}';
    }
}

/*
Change logs:
Date        |       Author          |       Description
2022-10-12  |   Fangzheng Zhang     |    create class and init
2022-10-15  |   Fangzheng Zhang     |   Added DB table and colum info
 */