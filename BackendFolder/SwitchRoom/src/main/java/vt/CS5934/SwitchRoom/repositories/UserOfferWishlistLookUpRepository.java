package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.UserOfferModel;
import vt.CS5934.SwitchRoom.models.UserOfferWishlistLookUpModel;

import java.util.List;

public interface UserOfferWishlistLookUpRepository extends JpaRepository<UserOfferWishlistLookUpModel, Long> {
    List<UserOfferWishlistLookUpModel> findAllByUserId(Long userId);
    UserOfferWishlistLookUpModel findByWishlistItemId(Long wishlistItemId);
    void deleteByWishlistItemId(Long wishlistItemId);
}

