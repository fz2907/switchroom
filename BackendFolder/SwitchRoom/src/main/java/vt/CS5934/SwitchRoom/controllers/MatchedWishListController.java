package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.models.WishlistItemModel;
import vt.CS5934.SwitchRoom.services.WishlistPageService;


/**
 * The "@MatchedController" made the class into rest handle class
 * The "@RequestMapping("matched")" on the class level make it only react to url ".../example/..."
 */
@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("matchedWishList")
public class MatchedWishListController {

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    WishlistPageService wishlistPageService;

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * This function will handle the post function and change the JSON body into data class
     */
    @GetMapping("/{wishlistId}")
    public ResponseModel getWishList(@PathVariable long wishlistId) {
        logger.info("You reached the getWishList() function.");
        try{
            WishlistItemModel wishlist = wishlistPageService.getWishlistItemInfo(wishlistId);
            return new ResponseModel(
                    "This this a offer String response",
                    HttpStatus.OK,
                    wishlist);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get All ExampleModels, Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }
}
