package vt.CS5934.SwitchRoom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.models.ExampleModel;
import vt.CS5934.SwitchRoom.models.MatchedWishlistRecordModel;
import vt.CS5934.SwitchRoom.models.UserOfferWishlistLookUpModel;
import vt.CS5934.SwitchRoom.repositories.MatchedWishlistRecordRepository;
import vt.CS5934.SwitchRoom.repositories.UserOfferWishlistLookUpRepository;

import java.util.*;

@Service
public class OfferWishlistLookUpService {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(ExampleService.class);

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    UserOfferWishlistLookUpRepository userOfferWishlistLookUpRepository;

    public List<UserOfferWishlistLookUpModel> getOfferWishlistLookUpWithIDFromDB(long id){
        logger.info("Reached getWishlistLookUpWithIDFromDB()");
        return userOfferWishlistLookUpRepository.findAllByUserId(id);
    }
}
