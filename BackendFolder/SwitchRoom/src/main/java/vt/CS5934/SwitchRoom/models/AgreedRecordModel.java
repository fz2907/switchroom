package vt.CS5934.SwitchRoom.models;


import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;
import java.util.concurrent.BlockingDeque;

@Entity
@Table(name = "success_record_table")
@NoArgsConstructor
public class AgreedRecordModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long stateCityCode;
    private String state;
    private String city;
    private Long wishlistId;
    private Long wishlistUserId;
    private Long offerId;
    private Date startTime;
    private Date endTime;
    private Integer toOfferStar = 5;
    private Integer toVisitorStar = 5;
    @Column(length = 500)
    private String toOfferComment;
    @Column(length = 500)
    private String toVisitorComment;
    private boolean onGoing = true;

    public AgreedRecordModel(Long stateCityCode, String state, String city, Long wishlistId, Long wishlistUserId,
                             Long offerId, Date startTime,
                             Date endTime, Integer toOfferStar, Integer toVisitorStar, String toOfferComment,
                             String toVisitorComment) {
        this.stateCityCode = stateCityCode;
        this.state = state;
        this.city = city;
        this.wishlistId = wishlistId;
        this.wishlistUserId = wishlistUserId;
        this.offerId = offerId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.toOfferStar = toOfferStar;
        this.toVisitorStar = toVisitorStar;
        this.toOfferComment = toOfferComment;
        this.toVisitorComment = toVisitorComment;
    }

    public Long getStateCityCode() {
        return stateCityCode;
    }

    public void setStateCityCode(Long stateCityCode) {
        this.stateCityCode = stateCityCode;
    }

    public Long getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(Long wishlistId) {
        this.wishlistId = wishlistId;
    }

    public Long getWishlistUserId() {
        return wishlistUserId;
    }

    public void setWishlistUserId(Long wishlistUserId) {
        this.wishlistUserId = wishlistUserId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getToOfferStar() {
        return toOfferStar;
    }

    public void setToOfferStar(Integer toOfferStar) {
        this.toOfferStar = toOfferStar;
    }

    public Integer getToVisitorStar() {
        return toVisitorStar;
    }

    public void setToVisitorStar(Integer toVisitorStar) {
        this.toVisitorStar = toVisitorStar;
    }

    public String getToOfferComment() {
        return toOfferComment;
    }

    public void setToOfferComment(String toOfferComment) {
        this.toOfferComment = toOfferComment;
    }

    public String getToVisitorComment() {
        return toVisitorComment;
    }

    public void setToVisitorComment(String toVisitorComment) {
        this.toVisitorComment = toVisitorComment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isOnGoing() {
        return onGoing;
    }

    public void setOnGoing(boolean onGoing) {
        this.onGoing = onGoing;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
