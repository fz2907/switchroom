package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "matched_wishlist_record_table")
@IdClass(MatchedRecordIdModel.class)
@NoArgsConstructor
public class MatchedWishlistRecordModel {
    @Id
    private Long offerId;
    @Id
    private Long wishlistItemId;
    private LookupTables.MATCHING_RECORD_USER_RESULT offerResult;
    private LookupTables.MATCHING_RECORD_USER_RESULT wishlistResult;
    private Date startTime;
    private Date endTime;
    private Long agreedRecordId;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private Date modifyDate;

    public MatchedWishlistRecordModel(Long wishlistItemId, Long offerId,
                                      LookupTables.MATCHING_RECORD_USER_RESULT offerResult,
                                      LookupTables.MATCHING_RECORD_USER_RESULT wishlistResult) {
        this.wishlistItemId = wishlistItemId;
        this.offerId = offerId;
        this.offerResult = offerResult;
        this.wishlistResult = wishlistResult;
    }

    public MatchedWishlistRecordModel(Long offerId, Long wishlistItemId,
                                      LookupTables.MATCHING_RECORD_USER_RESULT offerResult,
                                      LookupTables.MATCHING_RECORD_USER_RESULT wishlistResult,
                                      Date startTime, Date endTime) {
        this.offerId = offerId;
        this.wishlistItemId = wishlistItemId;
        this.offerResult = offerResult;
        this.wishlistResult = wishlistResult;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Long getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(Long wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public LookupTables.MATCHING_RECORD_USER_RESULT getOfferResult() {
        return offerResult;
    }

    public void setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT offerResult) {
        this.offerResult = offerResult;
    }

    public LookupTables.MATCHING_RECORD_USER_RESULT getWishlistResult() {
        return wishlistResult;
    }

    public void setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT wishlistResult) {
        this.wishlistResult = wishlistResult;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getAgreedRecordId() {
        return agreedRecordId;
    }

    public void setAgreedRecordId(Long agreedRecordId) {
        this.agreedRecordId = agreedRecordId;
    }

    public Date getModifyDate() {
        return modifyDate;
    }
}
