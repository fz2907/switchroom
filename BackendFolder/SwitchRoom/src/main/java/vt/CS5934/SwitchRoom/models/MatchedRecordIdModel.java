package vt.CS5934.SwitchRoom.models;

import java.io.Serializable;
import java.util.Objects;

public class MatchedRecordIdModel implements Serializable {

    private Long offerId;
    private Long wishlistItemId;

    public MatchedRecordIdModel(){}
    public MatchedRecordIdModel(Long offerId, Long wishlistItemId) {
        this.offerId = offerId;
        this.wishlistItemId = wishlistItemId;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Long getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(Long wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchedRecordIdModel that = (MatchedRecordIdModel) o;
        return Objects.equals(offerId, that.offerId) && Objects.equals(wishlistItemId, that.wishlistItemId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(offerId, wishlistItemId);
    }
}
