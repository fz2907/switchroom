package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "state_city_lookup_table")
@NoArgsConstructor
public class StateCityLookupModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stateCityCode;
    private Integer stateCode;
    private String cityName;

    public StateCityLookupModel(Long stateCityCode, Integer stateCode, String cityName) {
        this.stateCityCode = stateCityCode;
        this.stateCode = stateCode;
        this.cityName = cityName;
    }

    public Long getStateCityCode() {
        return stateCityCode;
    }

    public void setStateCityCode(Long stateCityCode) {
        this.stateCityCode = stateCityCode;
    }

    public Integer getStateCode() {
        return stateCode;
    }

    public void setStateCode(Integer stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
