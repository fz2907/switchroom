package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "wishlist_waiting_match_table")
@NoArgsConstructor
public class WishlistWaitingMatchModel {

    @Id
    private Long wishlistItemId;
    private Date startTime;
    private Date endTime;
    private Long stateCityCode;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private java.util.Date modifyDate;

    public WishlistWaitingMatchModel(Long wishlistItemId, Date startTime, Date endTime, Long stateCityCode) {
        this.wishlistItemId = wishlistItemId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.stateCityCode = stateCityCode;
    }

    public Long getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(Long wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getStateCityCode() {
        return stateCityCode;
    }

    public void setStateCityCode(Long stateCityCode) {
        this.stateCityCode = stateCityCode;
    }

    public java.util.Date getModifyDate() {
        return modifyDate;
    }
}
