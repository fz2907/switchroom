package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.MatchedRecordIdModel;
import vt.CS5934.SwitchRoom.models.MatchedWishlistRecordModel;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import java.util.Date;
import java.util.List;

public interface MatchedWishlistRecordRepository extends JpaRepository<MatchedWishlistRecordModel, MatchedRecordIdModel> {
    /**
     * The function name is the SQL query:
     *  findByIdAndName(long inputId, String inputName) == "SELETE * FROM table WHERE Id==inputId" AND name == inputName;
     * This is an example of how to declare a SQL command, those will be use in service class
     *
     * @param offerId the id in table you are looking for
     * @return ExampleModel object
     */
    List<MatchedWishlistRecordModel> findAllByOfferId(Long offerId);

    /**
     * The function name is the SQL query:
     * findByIdAndName(long inputId, String inputName) == "SELETE * FROM table WHERE Id==inputId" AND name == inputName;
     * This is an example of how to declare a SQL command, those will be use in service class
     *
     * @param wishlistItemId the id in table you are looking for
     * @return ExampleModel object
     */
    List<MatchedWishlistRecordModel> findAllByWishlistItemId(Long wishlistItemId);

    void deleteAllByOfferId(Long offerId);

    void deleteAllByWishlistItemId(Long wishlistItemId);

    Long countByWishlistItemId(Long wishlistItemId);
    MatchedWishlistRecordModel findByOfferIdAndWishlistItemId(Long offerId, Long wishlistId);
    List<MatchedWishlistRecordModel> findAllByOfferIdAndAgreedRecordIdIsNotNull(Long offerId);
    List<MatchedWishlistRecordModel> findAllByEndTimeBefore(Date todayDate);

    MatchedWishlistRecordModel findMatchedWishlistRecordModelByOfferIdAndWishlistItemId(Long offerId, Long wishlistItemId);

    void deleteByOfferIdAndWishlistItemId(Long offerId, Long wishlistItemId);

    void deleteMatchedWishlistRecordModelByOfferIdAndWishlistItemId(Long offerId, Long wishlistItemId);

    List<MatchedWishlistRecordModel> findAllByOfferIdAndOfferResult(Long offerId, LookupTables.MATCHING_RECORD_USER_RESULT offerResult);

    List<MatchedWishlistRecordModel> findAllByWishlistItemIdAndWishlistResult(Long wishlistItemId, LookupTables.MATCHING_RECORD_USER_RESULT wishlistResult);
}
