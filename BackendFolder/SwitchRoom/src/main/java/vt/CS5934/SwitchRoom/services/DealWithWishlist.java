package vt.CS5934.SwitchRoom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.models.MatchedWishlistRecordModel;
import vt.CS5934.SwitchRoom.models.UserOfferWishlistLookUpModel;
import vt.CS5934.SwitchRoom.repositories.MatchedWishlistRecordRepository;
import vt.CS5934.SwitchRoom.repositories.UserOfferWishlistLookUpRepository;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import java.util.ArrayList;
import java.util.List;

@Service
public class DealWithWishlist {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(ExampleService.class);

    @Autowired
    MatchedWishlistRecordRepository matchedWishlistRecordRepository;
    public List<MatchedWishlistRecordModel> getMatchedWishlist(List<UserOfferWishlistLookUpModel> list) {
        logger.info("Reached get finalAns");
        List<MatchedWishlistRecordModel> finalAns = new ArrayList<>();
        for(int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i).getWishlistItemId() + " come on");
            List<MatchedWishlistRecordModel> ans = matchedWishlistRecordRepository.findAllByWishlistItemIdAndWishlistResult(list.get(i).getWishlistItemId(), LookupTables.MATCHING_RECORD_USER_RESULT.Waiting);
//            System.out.println(ans);
            finalAns.addAll(ans);
        }
        return finalAns;
    }
//    public List<MatchedWishlistRecordModel> getMatchedWishlist(List<UserOfferWishlistLookUpModel> list) {
//        logger.info("Reached get finalAns");
//        List<MatchedWishlistRecordModel> finalAns = new ArrayList<>();
//        for(int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i).getWishlistItemId()+" come on");
//            List<MatchedWishlistRecordModel> ans = matchedWishlistRecordRepository.findAllByWishlistItemId(list.get(i).getWishlistItemId());
//            System.out.println(ans);
//            finalAns.addAll(ans);
//        }
//        return finalAns;
//    }

}
