/**
 * This class is a connection between offer table and wishlist table in DB
 */
package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "user_offer_wishlist_lookup_table")
@NoArgsConstructor
public class UserOfferWishlistLookUpModel {

    @Id
    private Long wishlistItemId;
    private Long userId;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private Date modifyDate;

    public UserOfferWishlistLookUpModel(Long userId, Long wishlistItemId) {
        this.userId = userId;
        this.wishlistItemId = wishlistItemId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWishlistItemId() {
        return wishlistItemId;
    }

    public void setWishlistItemId(Long wishlistId) {
        this.wishlistItemId = wishlistId;
    }

}

/*
Change logs:
Date        |       Author          |   Description
2022-10-21  |    Fangzheng Zhang    |    create class and init
2022-10-30  |   Fangzheng Zhang     |   Add modify_date into DB. Remove matchingOfferId, move it to a match result class
 */