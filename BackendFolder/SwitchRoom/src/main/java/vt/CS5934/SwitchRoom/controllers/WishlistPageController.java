package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.OfferMatchRequestInfo;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.models.WishlistItemModel;
import vt.CS5934.SwitchRoom.services.WishlistPageService;

import java.util.List;

@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("wishlist")
public class WishlistPageController {

    private final Logger logger = LoggerFactory.getLogger(WishlistPageController.class);

    @Autowired
    WishlistPageService wishlistPageService;

    @GetMapping
    public ResponseModel getWishlistList(@CookieValue(value = "userId") Long userId){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(wishlistPageService.getWishlistList(userId));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getWishlistList: "+e);
            responseModel.setMessage("getWishlistList Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @PostMapping("/newWishlistItem")
    public ResponseModel saveNewWishlistItem(@CookieValue(value = "userId") Long userId,
                                             @RequestBody WishlistItemModel wishlistItemModel){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(wishlistPageService.saveNewWishlistItem(userId, wishlistItemModel));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in saveNewWishlistItem: "+e);
            responseModel.setMessage("saveNewWishlistItem Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @PostMapping("/updateWishlistItem")
    public ResponseModel updateChangedWishlistItem(@RequestBody WishlistItemModel wishlistItemModel){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(wishlistPageService.updateWishlistItem(wishlistItemModel));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in updateChangedWishlistItem: "+e);
            responseModel.setMessage("updateChangedWishlistItem Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @DeleteMapping("/deleteWishlistItem/{wishlistItemId}")
    public ResponseModel deleteWishlistItem(@PathVariable Long wishlistItemId){
        ResponseModel responseModel = new ResponseModel();
        try{
            Long userId = wishlistPageService.getPairedUserId(wishlistItemId);
            wishlistPageService.deleteWishlistItem(wishlistItemId);
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(wishlistPageService.getWishlistList(userId));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in deleteWishlistItem: "+e);
            responseModel.setMessage("deleteWishlistItem Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @GetMapping("/loadOfferMatchList/{wishlistItemId}")
    public ResponseModel getOfferMatchList(@PathVariable Long wishlistItemId){
        ResponseModel responseModel = new ResponseModel();
        try{
            List<OfferMatchRequestInfo> offerMatchRequestInfoList =
                    wishlistPageService.getOfferMatchList(wishlistItemId);
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(offerMatchRequestInfoList);
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getOfferMatchList: "+e.fillInStackTrace());
            responseModel.setMessage("getOfferMatchList Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }
    @PostMapping("/loadOfferMatchCount")
    public ResponseModel getOfferMatchCount(@RequestBody List<Long> wishlistItemIdList){
        ResponseModel responseModel = new ResponseModel();
        try{
            List<Long> offerMatchCount = wishlistPageService.getOfferMatchCount(wishlistItemIdList);
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(offerMatchCount);
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getOfferMatchCount: "+e.fillInStackTrace());
            responseModel.setMessage("getOfferMatchCount Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }
}

/*
Change logs:
Date        |       Author          |   Description
2022-10-21  |    Fangzheng Zhang    |    create class and init
2022-11-03  |    Fangzheng Zhang    |   Remove user id in the parameter, since cookie is ready
2022-11-05  |    Fangzheng Zhang    |   add getOfferMatchList function and getOfferMatchCount function
 */