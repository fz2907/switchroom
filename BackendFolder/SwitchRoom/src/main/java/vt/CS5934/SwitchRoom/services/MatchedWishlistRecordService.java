package vt.CS5934.SwitchRoom.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vt.CS5934.SwitchRoom.models.*;
import vt.CS5934.SwitchRoom.repositories.*;
import vt.CS5934.SwitchRoom.utility.LookupTables;
import vt.CS5934.SwitchRoom.models.MatchedWishlistRecordModel;
import vt.CS5934.SwitchRoom.repositories.MatchedWishlistRecordRepository;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class MatchedWishlistRecordService {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(MatchedWishlistRecordService.class);

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    MatchedWishlistRecordRepository matchedWishlistRecordRepository;
    @Autowired
    AgreedRecordRepository agreedRecordRepository;
    @Autowired
    UserOfferRepository userOfferRepository;
    @Autowired
    UserOfferWishlistLookUpRepository userOfferWishlistLookUpRepository;
    @Autowired
    WishlistWaitingMatchRepository wishlistWaitingMatchRepository;
    @Autowired
    WishlistItemRepository wishlistItemRepository;


    public List<MatchedWishlistRecordModel> getOfferListWithIDFromDB(long id){
        logger.info("Reached getOfferListIDFromDB()");
        return matchedWishlistRecordRepository.findAllByOfferIdAndOfferResult(id, LookupTables.MATCHING_RECORD_USER_RESULT.Waiting);
    }

    public MatchedWishlistRecordModel getHistoryFromDB(Long offerId, Long wishlistItemId) {
        logger.info("Reached getHistoryFromDB()");
        return matchedWishlistRecordRepository.findMatchedWishlistRecordModelByOfferIdAndWishlistItemId(offerId, wishlistItemId);
    }

    public void deleteFromDB(Long offerId, Long wishlistItemId) {
        logger.info("Reached deleteFromDB()");

        matchedWishlistRecordRepository.deleteByOfferIdAndWishlistItemId(offerId, wishlistItemId);
    }

    public MatchedWishlistRecordModel updateToDB(MatchedWishlistRecordModel updateData) {
        updateData = matchedWishlistRecordRepository.save(updateData);
        if(updateData.getOfferResult() == LookupTables.MATCHING_RECORD_USER_RESULT.Accepted &&
                updateData.getWishlistResult() == LookupTables.MATCHING_RECORD_USER_RESULT.Accepted){
            visitorAccepted(updateData);
        }
        return updateData;
    }

    public void hostAcceptedOrUpdate(Long offerId, Long wishlistId, Date startTime, Date endTime){
        MatchedWishlistRecordModel record = matchedWishlistRecordRepository
                .findByOfferIdAndWishlistItemId(offerId, wishlistId);
        if(record != null){
            record.setStartTime(startTime);
            record.setEndTime(endTime);
            record.setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT.Accepted);
            matchedWishlistRecordRepository.save(record);
        }else{
            logger.error("Can not find the matched record with offerId: {}, and wishlistItemId:{}",
                    offerId, wishlistId);
        }
    }

    public void hostReject(MatchedWishlistRecordModel record){
        Long wishlistId = record.getWishlistItemId();
        Long offerId = record.getOfferId();
        if(record != null){
            record.setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT.Declined);
            if(record.getAgreedRecordId() != null){
                agreedRecordRepository.deleteById(record.getAgreedRecordId());
                wishlistItemRepository.save(wishlistItemRepository.findByWishlistItemId(wishlistId));
                record.setAgreedRecordId(null);
            }
            matchedWishlistRecordRepository.save(record);
        }else{
            logger.error("Can not find the matched record with offerId: {}, and wishlistItemId:{}",
                    offerId, wishlistId);
        }
    }

    public void visitorReject(MatchedWishlistRecordModel record){
        Long wishlistId = record.getWishlistItemId();
        Long offerId = record.getOfferId();
        if(record != null){
            record.setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT.Declined);
            if(record.getAgreedRecordId() != null){
                agreedRecordRepository.deleteById(record.getAgreedRecordId());
                wishlistItemRepository.save(wishlistItemRepository.findByWishlistItemId(wishlistId));
                record.setAgreedRecordId(null);
            }
            matchedWishlistRecordRepository.save(record);
        }else{
            logger.error("Can not find the matched record with offerId: {}, and wishlistItemId:{}",
                    offerId, wishlistId);
        }
    }

    public void visitorAccepted(MatchedWishlistRecordModel record){
        Long offerId = record.getOfferId();
        Long wishlistId = record.getWishlistItemId();
        if(record != null){
            record.setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT.Accepted);
            UserOfferModel offerModel = userOfferRepository.findByUserId(offerId);
            UserOfferWishlistLookUpModel offerWishLookup = userOfferWishlistLookUpRepository
                    .findByWishlistItemId(wishlistId);
            AgreedRecordModel agreedRecord = new AgreedRecordModel(
                    offerModel.getStateCityCode(), offerModel.getState(), offerModel.getSpaceLocateCity(),
                    wishlistId, offerWishLookup.getUserId(), offerId,
                    record.getStartTime(), record.getEndTime(), null, null,
                    null, null);
            agreedRecord = agreedRecordRepository.save(agreedRecord);
            record.setAgreedRecordId(agreedRecord.getId());
            matchedWishlistRecordRepository.save(record);
            wishlistWaitingMatchRepository.deleteAllByWishlistItemId(wishlistId);
            List<MatchedWishlistRecordModel> matchedWishlistRecords = matchedWishlistRecordRepository
                    .findAllByWishlistItemId(wishlistId)
                    .stream()
                    .filter(item-> !Objects.equals(item.getOfferId(), offerId))
                    .toList();
            matchedWishlistRecordRepository.deleteAll(matchedWishlistRecords);
        }else{
            logger.error("Can not find the matched record with offerId: {}, and wishlistItemId:{}",
                    offerId, wishlistId);
        }
    }
}
