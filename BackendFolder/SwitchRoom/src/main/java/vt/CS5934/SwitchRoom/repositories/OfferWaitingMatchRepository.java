package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vt.CS5934.SwitchRoom.models.OfferWaitingMatchModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface OfferWaitingMatchRepository extends JpaRepository<OfferWaitingMatchModel, Long> {
    List<OfferWaitingMatchModel> findAllByStateCityCodeAndModifyDateAfter(Long stateCityCode, Date lastPullTime);
    @Query()
    List<OfferWaitingMatchModel> findAllByStateCityCodeAndModifyDateBefore(Long stateCityCode, Date lastPullTime);

    List<OfferWaitingMatchModel> findAllByModifyDateAfter(Date lastPullTime);
    List<OfferWaitingMatchModel> findAllByModifyDateLessThanEqualAndStateCityCodeIn(Date lastPullTime, Set<Long> StateCityCodeSet);
    void deleteAllByOfferId(Long offerId);

}
