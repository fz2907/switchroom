package vt.CS5934.SwitchRoom.services;

import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import io.jsonwebtoken.Jwts;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Token {
    @Getter
    private final String token;

    private Token(String token) {
        this.token = token;
    }

    public static Token of(int userId, Long validityInMinutes, String secretKey) {
        var issueDate = Instant.now();
        return new Token(
                Jwts.builder()
                    .claim("user_id", userId)
                    .setIssuedAt(Date.from(issueDate))
                    .setExpiration(Date.from(issueDate.plus(validityInMinutes, ChronoUnit.MINUTES)))
                    .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact());
    }
}
