package vt.CS5934.SwitchRoom.models;

import lombok.Data;

import java.util.Date;

@Data
public class OfferMatchRequestInfo {
    private Long offerOwnerId;
    private Long wishListId;
    private Date startTime;
    private Date endTime;
    private Integer zipCode;
    private String hostName;

    public OfferMatchRequestInfo(Long offerOwnerId, Long wishListId, Date startTime, Date endTime,
                                 Integer zipCode, String hostName) {
        this.offerOwnerId = offerOwnerId;
        this.wishListId = wishListId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.zipCode = zipCode;
        this.hostName = hostName;
    }

    public Long getOfferOwnerId() {
        return offerOwnerId;
    }

    public void setOfferOwnerId(Long offerOwnerId) {
        this.offerOwnerId = offerOwnerId;
    }

    public Long getWishListId() {
        return wishListId;
    }

    public void setWishListId(Long wishListId) {
        this.wishListId = wishListId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
