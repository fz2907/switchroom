/**
 * This class is ResponseModel for http request response.
 */

package vt.CS5934.SwitchRoom.models;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ResponseModel {
    String message;
    HttpStatus status;
    Object data;

    public ResponseModel(){
        message = "";
        status = HttpStatus.NOT_ACCEPTABLE;
        data = null;
    }

    public ResponseModel(String message, HttpStatus status, Object data){
        this.message = message;
        this.status = status;
        this.data = data;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public void setStatus(HttpStatus status){
        this.status = status;
    }

    public void setMessage(Object data){
        this.data = data;
    }
}

/*
Change logs:
Date        |       Author          |   Description
2022-10-12  |    Fangzheng Zhang    |    create class and init
 */