package vt.CS5934.SwitchRoom.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.InteractionCombinedModel;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.models.UserModel;
import vt.CS5934.SwitchRoom.services.Token;
import vt.CS5934.SwitchRoom.services.UserService;
import vt.CS5934.SwitchRoom.utility.UsefulTools;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.NoSuchAlgorithmException;

/**
 * The "@RestController" made the class into rest handle class
 * The "@RequestMapping("example")" on the class level make it only react to url ".../example/..."
 */
@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    UserService userService;

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * This function will handle the post function and change the JSON body into data class
     * @param newUser the JSON input in the request body
     */
    @PostMapping("/newUser")
    public ResponseModel handlePost(@RequestBody UserModel newUser) throws NoSuchAlgorithmException {
        logger.info("You reached the handlePost() functions.");
        ResponseModel response = new ResponseModel();

        try{
            UserModel userModel = userService.addUserToDB(newUser.getUsername(), userService.hashPassword(newUser.getPassword()), newUser.getEmail(), newUser.getFirstname(), newUser.getLastname(), newUser.getGender());
            response.setMessage("Saved successfully");
            response.setStatus(HttpStatus.OK);
            response.setData(userModel);
            return response;
        }catch (Exception e){
            return new ResponseModel(
                    "Error happen",
                    HttpStatus.OK,
                    null);
        }
    }

    @PostMapping("/loginUser")
    public ResponseModel loginUser(@RequestBody UserModel userInfo, HttpServletResponse servletResponse)
            throws NoSuchAlgorithmException {
        ResponseModel response = new ResponseModel();

        String[] result = userService.loginUser(userInfo);

        if (result.length > 0) {
            Cookie theCookie = new Cookie("userId", result[0]);
            theCookie.setHttpOnly(false);
            theCookie.setSecure(false);
            theCookie.setPath("/");
            theCookie.setMaxAge(60*60); // 1 hour
            servletResponse.addCookie(theCookie);


            Cookie theCookie2 = new Cookie("token", result[1]);
            theCookie2.setHttpOnly(false);
            theCookie2.setSecure(false);
            theCookie2.setPath("/");
            theCookie2.setMaxAge(60*60);
            servletResponse.addCookie(theCookie2);

            response.setMessage("Login in successfully");
            response.setStatus(HttpStatus.OK);
        } else {
            response.setMessage("Couldn't find an account matching the login info you entered");
            response.setStatus(HttpStatus.FORBIDDEN);
        }

        return response;
    }
    @GetMapping("/checkLoginSession")
    public ResponseModel checkLoginSession(
            @CookieValue(value = "userId", required = false) String userId,
            @CookieValue(value = "token", required = false) String token) {
        ResponseModel response = new ResponseModel();
        boolean result = userService.checkLoginSession(userId, token);

        if (result) {
            response.setStatus(HttpStatus.OK);
        } else {
            response.setMessage("Login session expired or invalid");
            response.setStatus(HttpStatus.FORBIDDEN);
        }

        return response;
    }

    @PostMapping("/resetPassword")
    public ResponseModel resetPassword(
            @CookieValue(value = "userId", required = false) String userId,
            @CookieValue(value = "token", required = false) String token,
            @RequestBody String payload) throws JsonProcessingException, NoSuchAlgorithmException {
        ResponseModel response = new ResponseModel();

        if (userId == null || token == null || !userService.checkLoginSession(userId, token)) {
            response.setMessage("Login session expired or invalid");
            response.setStatus(HttpStatus.FORBIDDEN);
            return response;
        }

        boolean result = userService.resetPassword(userId, payload);
        if (result) {
            response.setMessage("Successfully reset your password");
            response.setStatus(HttpStatus.OK);
        } else {
            response.setMessage("Couldn't find an account matching the login info you entered");
            response.setStatus(HttpStatus.FORBIDDEN);
        }

        return response;
    }

    @GetMapping("/profile")
    public ResponseModel getProfile(
            @CookieValue(value = "userId", required = false) String userId,
            @CookieValue(value = "token", required = false) String token) {
        ResponseModel response = new ResponseModel();

        if (userId == null || token == null || !userService.checkLoginSession(userId, token)) {
            response.setMessage("Login session expired or invalid");
            response.setStatus(HttpStatus.FORBIDDEN);
            return response;
        }

        response = userService.getProfile(userId);
        return response;
    }

    @PostMapping("/forgotPassword")
    public ResponseModel processForgotPassword(
            HttpServletRequest request,
            @RequestBody String payload
    ) throws MessagingException, UnsupportedEncodingException, JsonProcessingException {
        ResponseModel response = new ResponseModel();
        boolean result = userService.updateResetPasswordToken(payload, request);

        if (result) {
            response.setStatus(HttpStatus.OK);
            response.setMessage("We have sent a reset password link to your email. Please check.");
        } else {
            response.setMessage("Couldn't find an account matching the email you entered");
            response.setStatus(HttpStatus.FORBIDDEN);
        }

        return response;
    }

    @GetMapping("/forgotPassword_verify")
    public ResponseEntity<Void> showResetPasswordForm(
            @Param(value = "token") String token,
            HttpServletRequest request,
            HttpServletResponse servletResponse
    ) {
        UserModel existUser = userService.getByResetPasswordToken(token);
        String baseURL = UsefulTools.getSiteURL(request);

        // deployed
//        if (existUser != null) {
//            return ResponseEntity.status(HttpStatus.FOUND)
//                    .location(URI.create(baseURL + "/forgotPasswordForm"))
//                    .build();
//        } else {
//            return ResponseEntity.status(HttpStatus.FOUND)
//                    .location(URI.create(baseURL))
//                    .build();
//        }

        // development
        if (existUser != null) {
            Cookie theCookie = new Cookie("resetToken", token);
            theCookie.setHttpOnly(false);
            theCookie.setSecure(false);
            theCookie.setPath("/");
            theCookie.setMaxAge(60*60); // 1 hour
            servletResponse.addCookie(theCookie);
            return ResponseEntity.status(HttpStatus.FOUND)
                    .location(URI.create("http://localhost:8080/forgotPasswordForm"))
                    .build();
        } else {
            return ResponseEntity.status(HttpStatus.FOUND)
                    .location(URI.create("http://localhost:8080"))
                    .build();
        }
    }

    @PostMapping("/forgotPasswordCreateNew")
    public ResponseModel forgotPasswordCreateNew(
            @CookieValue(value = "resetToken", required = false) String token,
            @RequestBody String payload) throws JsonProcessingException, NoSuchAlgorithmException {
        ResponseModel response = new ResponseModel();

        if (token == null) {
            response.setMessage("Login session expired or invalid");
            response.setStatus(HttpStatus.FORBIDDEN);
            return response;
        }

        boolean result = userService.forgotPasswordCreateNew(token, payload);
        if (result) {
            response.setMessage("Successfully reset your password");
            response.setStatus(HttpStatus.OK);
        } else {
            response.setMessage("Couldn't find an account matching your login session");
            response.setStatus(HttpStatus.FORBIDDEN);
        }

        return response;
    }

    @GetMapping("/{userId}")
    public ResponseModel getSpecificUser(@PathVariable Integer userId) {
        logger.info("You reached the getSpecificUser() function.");
        System.out.println("this is the specific user id " + userId);
        try{
            UserModel back = userService.findSpecificUser(userId);
            return new ResponseModel(
                    "This is a Interaction array response",
                    HttpStatus.OK,
                    back);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get All ExampleModels, Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }
}
