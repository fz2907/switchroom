package vt.CS5934.SwitchRoom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SwitchRoomApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwitchRoomApplication.class, args);
	}

}

/*
Change logs:
Date        |       Author          |   Description
2022-10-31  |   Fangzheng Zhang     |   Add EnableScheduling to build the scheduled task

 */