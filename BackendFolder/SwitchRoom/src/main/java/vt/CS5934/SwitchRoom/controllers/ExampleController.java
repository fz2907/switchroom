/**
 * This class is an example class that show the functionalities that spring controller can do.
 * You can copy the functions and use in you class.
 */
package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.ExampleModel;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.services.ExampleService;

import java.util.List;

/**
 * The "@RestController" made the class into rest handle class
 * The "@RequestMapping("example")" on the class level make it only react to url ".../example/..."
 */
@CrossOrigin
@RestController
@RequestMapping("example")
public class ExampleController {
    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    ExampleService exampleService;

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(ExampleController.class);

    /**
     * This function handles GET request on url "/example"
     * @return a ResponseModel in String format
     */
    @GetMapping
    public ResponseModel getExample() {
        logger.info("You reached the getExample() function.");
        try{
            List<ExampleModel> exampleModels = exampleService.getAllExampleModelsFromDB();
            return new ResponseModel(
                    "This this a example String response",
                    HttpStatus.OK,
                    exampleModels);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get All ExampleModels, Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }

    /**
     * This function handles GET request on url "/example/<a-long-val>"
     * @param userId this variable will be passed in the url path
     * @return a ResponseModel in String format
     */
    @GetMapping("/{userId}")
    public ResponseModel getExampleWithInput(@PathVariable long userId){
        try{
            ExampleModel exampleModel = exampleService.getExampleModelWithIDFromDB(userId);
            return new ResponseModel(
                    "This this a example String response",
                    HttpStatus.OK,
                    exampleModel);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get ExampleModel with user_id == " + userId
                            + "Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }

    /**
     * This function handles GET request on url "/example/detail/<a-long-val>"
     * @param userId this variable will be passed in the url path
     * @return a ResponseModel in String format
     */
    @GetMapping("/detail/{userId}")
    public ResponseModel getExampleDetailWithInput(@PathVariable long userId){

        return new ResponseModel(
                "This this a example String response",
                HttpStatus.OK,
                "You GET this function with detail id: "+ userId);
    }

    /**
     * This function handles GET request on url "/example/getUser"
     * @return an example user info in the data field
     */
    @RequestMapping("/getUser")
    public ResponseModel getExampleUser(){
        ExampleModel user = new ExampleModel( "example-user");
        return new ResponseModel(
                "There is your example user info",
                HttpStatus.OK,
                user);
    }


    /**
     * This function will handle the post function and change the JSON body into data class
     * @param newUser the JSON input in the request body
     */
    @PostMapping("/newUser")
    public ResponseModel handlePost(@RequestBody ExampleModel newUser){
        logger.info("You reached the handlePost() function.");

        System.out.println(newUser.getUsername());
        ResponseModel response = new ResponseModel();

        try{
            ExampleModel newModel = exampleService.addNewExampleModelToDB(newUser.getUsername());
            response.setMessage("Saved successfully");
            response.setStatus(HttpStatus.OK);
            response.setData(newModel);
            return response;
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on save new example model to DB, model: " + newUser
                            + " Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }

    /**
     * If you need to delete anything from DB, use DELETE request
     * @param userId user id
     * @return info
     */
    @DeleteMapping("/logout/{userId}")
    public ResponseModel logout(@PathVariable long userId){
        logger.warn("You reached the logout() function.");
        ResponseModel response = new ResponseModel();
        try{
            response.setStatus(HttpStatus.OK);
            if(exampleService.removeExampleModelWithId(userId)){
                response.setMessage("Successfully Delete ExampleModel with ID: " + userId);
            }else{
                response.setMessage("Unable to Delete ExampleModel with ID: " + userId);
            }
            return response;
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on delete ExampleModel with by: " + userId
                            + " Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }


    }
}

/*
Change logs:
Date        |       Author          |   Description
2022-10-11  |    Fangzheng Zhang    |    create class and init
 */
