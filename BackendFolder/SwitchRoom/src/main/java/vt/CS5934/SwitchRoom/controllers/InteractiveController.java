package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.*;
import vt.CS5934.SwitchRoom.services.InteractionService;
import vt.CS5934.SwitchRoom.services.MatchedWishlistRecordService;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import java.util.Map;

/**
 * The "@RestController" made the class into rest handle class
 * The "@RequestMapping("example")" on the class level make it only react to url ".../example/..."
 */
@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("interactive")
public class InteractiveController {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    InteractionService interactionService;

    @Autowired
    MatchedWishlistRecordService matchedWishlistRecordService;

    /**
     * This function will handle the post function and change the JSON body into data class
     * @param newData the JSON input in the request body
     */
    @PostMapping("/newData")
    public ResponseModel handlePost(@RequestBody Map<String, Object> newData) {
        logger.info("You reached the handlePost() functions.");
        System.out.println(newData);
        ResponseModel response = new ResponseModel();
        InteractionModel back = null;
        try{
            InteractionModel result = interactionService.searchBothIdFromDB(newData.get("offerId").toString(), newData.get("wishlistId").toString());
            if(result == null) {
                back = interactionService.addDataToDB(Integer.parseInt(newData.get("userId").toString()), null, newData.get("phone").toString(), null, false, newData.get("offerId").toString(), newData.get("wishlistId").toString());
                if("offer".equals(newData.get("see").toString())) {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT.Accepted);
                    matchedWishlistRecordService.updateToDB(history);
                } else {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT.Accepted);
                    matchedWishlistRecordService.updateToDB(history);
                }
            } else {
                if(result.getPhone1() == null) {
                    result.setUserId2(Integer.parseInt(newData.get("userId").toString()));
                    result.setPhone2(newData.get("phone").toString());
                    result.setJudgement(false);
                    InteractionModel resultAfterUpdate = interactionService.updateToDB(result);
                    back = resultAfterUpdate;
                } else {
                    result.setPhone2(newData.get("phone").toString());
                    result.setJudgement(true);
                    result.setUserId2(Integer.parseInt(newData.get("userId").toString()));
                    InteractionModel resultAfterUpdate = interactionService.updateToDB(result);
                    back = resultAfterUpdate;
                    System.out.println("what is that?");
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    System.out.println("why this happened " + history.toString());
                }
                if("offer".equals(newData.get("see").toString())) {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT.Accepted);
                    matchedWishlistRecordService.updateToDB(history);
                } else {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT.Accepted);
                    matchedWishlistRecordService.updateToDB(history);
                }
            }
            response.setMessage("Saved successfully");
            response.setStatus(HttpStatus.OK);
            response.setData(back);
            return response;
        }catch (Exception e){
            return new ResponseModel(
                    "Error happen",
                    HttpStatus.OK,
                    null);
        }
    }

    /**
     * This function will handle the post function and change the JSON body into data class
     */
    @GetMapping("/{userId}")
    public ResponseModel getInteraction(@PathVariable Integer userId) {
        logger.info("You reached the getInteraction() function.");
        try{
//            InteractionModel[] tranBack = interactionService.findAllInteractionsFromDB(userId);
            InteractionCombinedModel tranBack = interactionService.findAllInteractionsFromDB(userId);
            return new ResponseModel(
                    "This is a Interaction array response",
                    HttpStatus.OK,
                    tranBack);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get All ExampleModels, Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }

    /**
     * This function will handle the post function and change the JSON body into data class
     * @param newData the JSON input in the request body
     */
    @PostMapping("/disagree")
    public ResponseModel disagreePost(@RequestBody Map<String, Object> newData) {
        logger.info("You reached the disagreePost() functions.");
        ResponseModel response = new ResponseModel();
        InteractionModel back;
        try{
            InteractionModel result = interactionService.searchBothIdFromDB(newData.get("offerId").toString(), newData.get("wishlistId").toString());
            System.out.println(result);
            if(result == null) {
                back = interactionService.addDataToDB(Integer.parseInt(newData.get("userId").toString()), null, null, null, false, newData.get("offerId").toString(), newData.get("wishlistId").toString());
                if("offer".equals(newData.get("see").toString())) {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT.Declined);
                    MatchedWishlistRecordModel record = matchedWishlistRecordService.updateToDB(history);
                    matchedWishlistRecordService.hostReject(record);
                } else {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT.Declined);
                    MatchedWishlistRecordModel record = matchedWishlistRecordService.updateToDB(history);
                    matchedWishlistRecordService.visitorReject(record);
                }
            } else {
                if(result.getPhone1() == null) {
                    result.setUserId2(Integer.parseInt(newData.get("userId").toString()));
                    InteractionModel resultAfterUpdate = interactionService.updateToDB(result);
                    back = resultAfterUpdate;
                } else {
                    result.setUserId2(Integer.parseInt(newData.get("userId").toString()));
                    InteractionModel resultAfterUpdate = interactionService.updateToDB(result);
                    back = resultAfterUpdate;
                }
                if("offer".equals(newData.get("see").toString())) {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setOfferResult(LookupTables.MATCHING_RECORD_USER_RESULT.Declined);
                    MatchedWishlistRecordModel record = matchedWishlistRecordService.updateToDB(history);
                    matchedWishlistRecordService.hostReject(record);
                } else {
                    MatchedWishlistRecordModel history = matchedWishlistRecordService.getHistoryFromDB(Long.parseLong(newData.get("offerId").toString()), Long.parseLong(newData.get("wishlistId").toString()));
                    history.setWishlistResult(LookupTables.MATCHING_RECORD_USER_RESULT.Declined);
                    MatchedWishlistRecordModel record = matchedWishlistRecordService.updateToDB(history);
                    matchedWishlistRecordService.visitorReject(record);
                }
            }
            response.setMessage("Saved successfully");
            response.setStatus(HttpStatus.OK);
            response.setData(back);
            return response;
        }catch (Exception e){
            return new ResponseModel(
                    "Error happen",
                    HttpStatus.OK,
                    null);
        }
    }
}
