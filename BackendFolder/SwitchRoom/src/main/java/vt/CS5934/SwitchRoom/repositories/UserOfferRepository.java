package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.UserOfferModel;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

public interface UserOfferRepository extends JpaRepository<UserOfferModel, Long> {

    UserOfferModel findByUserId(Long userId);
    void deleteByUserId(Long userId);
    List<UserOfferModel> findAllByOfferingAndModifyDateAfter(Boolean offering, Date modifyDate);
}
