package vt.CS5934.SwitchRoom.models;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * '@Data' tell spring this class can be map to JSON object if needed
 * '@Entity' declare this class is a DB table
 * '@Table(name=<table_name>)' declared which table stores its data
 */
@Data
@Entity
@Table(name = "User")
@NoArgsConstructor
public class UserModel {
    /**
     * '@Id' declare that userId object is the primary id in this table
     * '@Column' you can set the table column name in the DB
     */
    @Id
    @Column(name="user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    @Column(name="username",unique=true)
    private String username;

    @Column(name="password", length = 1000)
    private String password;

    @Column(name="email")
    private String email;

    @Column(name="firstname")
    private String firstname;

    @Column(name="lastname")
    private String lastname;

    @Column(name="gender")
    private String gender;

    @Column(name="token")
    private String token;

    @Column(name="reset_password_token")
    private String resetPasswordToken;

    public UserModel(
            String name,
            String password,
            String email,
            String firstname,
            String lastname,
            String gender,
            String token,
            String resetPasswordToken) {
        this.username = name;
        this.password = password;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.token = token;
        this.resetPasswordToken = resetPasswordToken;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", gender='" + gender + '\'' +
                ", token='" + token + '\'' +
                ", resetPasswordToken='" + resetPasswordToken + '\'' +
                '}';
    }

}
