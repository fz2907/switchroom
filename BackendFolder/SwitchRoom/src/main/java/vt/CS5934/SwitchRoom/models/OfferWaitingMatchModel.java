package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "offer_waiting_match_table")
@NoArgsConstructor
public class OfferWaitingMatchModel {
    @Id
    private Long offerId;
    private Date startTime;
    private Date endTime;
    private Long stateCityCode;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private java.util.Date modifyDate;

    public OfferWaitingMatchModel(Long offerId, Date startTime, Date endTime, Long stateCityCode) {
        this.offerId = offerId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.stateCityCode = stateCityCode;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Long getStateCityCode() {
        return stateCityCode;
    }

    public void setStateCityCode(Long stateCityCode) {
        this.stateCityCode = stateCityCode;
    }

    public java.util.Date getModifyDate() {
        return modifyDate;
    }

}
