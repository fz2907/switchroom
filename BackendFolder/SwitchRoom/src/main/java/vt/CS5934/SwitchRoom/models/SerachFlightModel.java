package vt.CS5934.SwitchRoom.models;

import java.util.Date;

public class SerachFlightModel {

    private String departure;

    private String destination;

    private Date date1;

    private Date date2;

    private int numAdults;

    private int numSeniors;

    private String classOfService;


    public SerachFlightModel(String departure, String destination, Date date1, Date date2, int numAdults, int numSeniors, String classOfService) {
        this.departure = departure;
        this.destination = destination;
        this.date1 = date1;
        this.date2 = date2;
        this.numAdults = numAdults;
        this.numSeniors = numSeniors;
        this.classOfService = classOfService;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public int getNumAdults() {
        return numAdults;
    }

    public void setNumAdults(int numAdults) {
        this.numAdults = numAdults;
    }

    public int getNumSeniors() {
        return numSeniors;
    }

    public void setNumSeniors(int numSeniors) {
        this.numSeniors = numSeniors;
    }

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }
}