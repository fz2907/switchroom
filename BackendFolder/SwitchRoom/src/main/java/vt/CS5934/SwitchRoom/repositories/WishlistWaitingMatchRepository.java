package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vt.CS5934.SwitchRoom.models.WishlistWaitingMatchModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface WishlistWaitingMatchRepository extends JpaRepository<WishlistWaitingMatchModel, Long> {
    @Query("SELECT DISTINCT item.stateCityCode FROM WishlistWaitingMatchModel item WHERE item.modifyDate > ?1")
    List<Long> findAllUniqueStateCityCodeAfter(Date lastPullTime);

    @Query("SELECT DISTINCT item.stateCityCode FROM WishlistWaitingMatchModel item WHERE item.modifyDate <= ?1")
    List<Long> findAllUniqueStateCityCodeBefore(Date lastPullTime);

    List<WishlistWaitingMatchModel> findAllByModifyDateAfter(Date lastPullTime);
    List<WishlistWaitingMatchModel> findAllByModifyDateLessThanEqualAndStateCityCodeIn(Date lastPullTime, Set<Long> stateCityCodeSet);

    void deleteAllByWishlistItemId(Long wishlistItemId);

}
