package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.AgreedRecordModel;

import java.util.Date;
import java.util.List;

public interface AgreedRecordRepository extends JpaRepository<AgreedRecordModel, Long> {

    List<AgreedRecordModel> findAllByWishlistUserId(Long wishlistUserId);
    List<AgreedRecordModel> findAllByOfferId(Long offerId);
    AgreedRecordModel findByOfferIdAndWishlistId(Long offerId, Long wishlistId);
    AgreedRecordModel findOneByOfferIdAndStartTimeGreaterThanEqualAndStartTimeLessThanEqualOrOfferIdAndEndTimeGreaterThanEqualAndEndTimeLessThanEqual(
            Long offerId1,Date startTime1, Date endTime1, Long offerId2,Date startTime2, Date endTime2);
    List<AgreedRecordModel> findAllByIdIn(List<Long> ids);
}
