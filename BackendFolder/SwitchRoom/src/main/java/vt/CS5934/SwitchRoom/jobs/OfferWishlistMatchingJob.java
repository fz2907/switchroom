/**
 * This class contains the business logic about find matches between offers and wishlist items
 */

package vt.CS5934.SwitchRoom.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import vt.CS5934.SwitchRoom.models.*;
import vt.CS5934.SwitchRoom.repositories.*;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static vt.CS5934.SwitchRoom.utility.UsefulTools.DATE_FORMAT;


@Component
@EnableAsync
@Async
public class OfferWishlistMatchingJob {

    private final Logger logger = LoggerFactory.getLogger(OfferWishlistMatchingJob.class);

    @Autowired
    MatchingJobInfoRepository matchingJobInfoRepository;
    @Autowired
    UserOfferRepository userOfferRepository;
    @Autowired
    OfferWaitingMatchRepository offerWaitingMatchRepository;
    @Autowired
    WishlistItemRepository wishlistItemRepository;
    @Autowired
    WishlistWaitingMatchRepository wishlistWaitingMatchRepository;
    @Autowired
    MatchedWishlistRecordRepository matchedWishlistRecordRepository;
    @Autowired
    AgreedRecordRepository agreedRecordRepository;

    @Scheduled(fixedDelayString = "${offer.wishlist.job.gap.seconds:60}000",
            initialDelayString = "${offer.wishlist.job.init.delay.seconds:60}000")
    public void jobStarter(){
        logger.info("Offer Wishlist Job start ...");
        HashMap<Long, MatchingJobInfoModel> lastPullTimeMap = new HashMap<>();
        lastPullTimeMap.put(LookupTables.OFFER_JOB_DB_ID, null);
        lastPullTimeMap.put(LookupTables.WISHLIST_JOB_DB_ID, null);
        lastPullTimeMap.put(LookupTables.MATCHING_JOB_DB_ID, null);
        List<OfferWaitingMatchModel> newOfferWaitingRecordList = null;
        List<WishlistWaitingMatchModel> newWishlistWaitingRecordList = null;

        try{
            //check the job status table, get the last pull time
            getLastPullTime(lastPullTimeMap);
            logger.info("getLastPullTime() finished");

            // Query and push the new offer records into its waiting list
            newOfferWaitingRecordList =
                    pushNewOfferRecordsIntoWaitingTable(lastPullTimeMap.get(LookupTables.OFFER_JOB_DB_ID));
            logger.info("pushNewOfferRecordsIntoWaitingTable() finished");

            // Query and push the new wishlist records into its waiting list
            newWishlistWaitingRecordList =
                    pushNewWishlistItemRecordsIntoWaitingTable(lastPullTimeMap.get(LookupTables.WISHLIST_JOB_DB_ID));
            logger.info("pushNewWishlistItemRecordsIntoWaitingTable() finished");

        }catch (Exception e){
            logger.error("Error occur when fetch the new records from Offer table / Wishlist table or" +
                    "push them into their waiting table: " +e.fillInStackTrace());
            // may need to stop the task if needed.
        }

        try{
            //find the match records and push to the matched table
            findMatchFromWaitingTable(lastPullTimeMap.get(LookupTables.MATCHING_JOB_DB_ID), newOfferWaitingRecordList,
                    newWishlistWaitingRecordList);
        }catch (Exception e){
            logger.error("Error occur when finding matches and push them to matched table: " +e.fillInStackTrace());
        }

    }

    @Scheduled(fixedDelayString = "${offer.wishlist.clear.old.job.gap.seconds}000",
            initialDelayString = "${offer.wishlist.job.init.delay.seconds:60}000")
    public void clearOldJobStarter(){
        logger.info("clearOldJobStarter() Started...");
        try{
            List<MatchedWishlistRecordModel> oldRecords = matchedWishlistRecordRepository
                    .findAllByEndTimeBefore(new Date());
            List<Long>agreedRecordIds = oldRecords.stream().map(MatchedWishlistRecordModel::getAgreedRecordId)
                    .collect(Collectors.toList());
            List<AgreedRecordModel> agreedRecordModels = agreedRecordRepository.findAllByIdIn(agreedRecordIds);
            agreedRecordModels.forEach(item->item.setOnGoing(false));
            agreedRecordRepository.saveAll(agreedRecordModels);
            matchedWishlistRecordRepository.deleteAll(oldRecords);
        }catch (Exception e){
            logger.error("Error occur when clear old matched records");
        }
    }


    /**
     * This function is to get the last pull time into a map, so the job knows the starting time range.
     * If DB do not have the job record info, it will build a default with starting of time as last pull time
     * @param lastPullTimeMap a map with job ids
     */
    private void getLastPullTime(HashMap<Long, MatchingJobInfoModel> lastPullTimeMap){
        logger.info("Starting getLastPullTime function");
        for(Long jobId : lastPullTimeMap.keySet()){
            MatchingJobInfoModel jobInfo = matchingJobInfoRepository.findByJobId(jobId);
            logger.info("Got job with id: {} info, the record is: {}", jobId, jobInfo);
            if(jobInfo == null){
                logger.info("Offer job info is null, building initial offer job info record");
                jobInfo = new MatchingJobInfoModel(jobId,LookupTables.JOB_STATUS.Running, new Date(0));
            }
            logger.info("The last pull time for job with id: {} is: {}",
                    jobId, DATE_FORMAT.format(jobInfo.getLastPullTime()));
            jobInfo.setJobStatus(LookupTables.JOB_STATUS.Running);
            jobInfo = matchingJobInfoRepository.save(jobInfo);
            lastPullTimeMap.put(jobId,jobInfo);
        }
    }

    /**
     * This function will pull the new Offer records from Offer_table and push them into offer_waiting_table
     * @param offerJobRecord offer job information to start the task
     */
    private List<OfferWaitingMatchModel> pushNewOfferRecordsIntoWaitingTable(MatchingJobInfoModel offerJobRecord){
        logger.info("Starting pushNewOfferRecordsIntoWaitingTable function with lastPullTime:{}"
                ,DATE_FORMAT.format(offerJobRecord.getLastPullTime()));

        // update the job info record time
        Date lastPullOfferTime = offerJobRecord.getLastPullTime();
        offerJobRecord.setJobStatus(LookupTables.JOB_STATUS.Done);
        offerJobRecord.setLastPullTime(new Date(System.currentTimeMillis()));

        // fetch the Offer records and push them to waiting table
        List<UserOfferModel> newOfferList = userOfferRepository
                .findAllByOfferingAndModifyDateAfter(true, lastPullOfferTime);
        logger.info("Fetched {} new Offer records, and set the last pull time to {}", newOfferList.size(),
                DATE_FORMAT.format(offerJobRecord.getLastPullTime()));

        List<OfferWaitingMatchModel> newOfferWaitingRecordList = newOfferList.parallelStream().map(offerRecord->
                new OfferWaitingMatchModel(offerRecord.getUserId(), offerRecord.getAvailableTimeStart(),
                        offerRecord.getAvailableTimeEnd(), offerRecord.getStateCityCode()))
                        .toList();
        offerWaitingMatchRepository.saveAll(newOfferWaitingRecordList);

        matchingJobInfoRepository.save(offerJobRecord);
        return newOfferWaitingRecordList;
    }

    /**
     * This function will pull the new wishlist records from wishlist_table and push them into wishlist_waiting_table
     * @param wishlistJobInfo wishlist job information to start the task
     */
    private List<WishlistWaitingMatchModel> pushNewWishlistItemRecordsIntoWaitingTable(MatchingJobInfoModel
                                                                                               wishlistJobInfo){
        logger.info("Starting pushNewWishlistItemRecordsIntoWaitingTable function with lastPullTime:{}"
                ,DATE_FORMAT.format(wishlistJobInfo.getLastPullTime()));

        // update the job info record time
        Date lastPullOfferTime = wishlistJobInfo.getLastPullTime();
        wishlistJobInfo.setJobStatus(LookupTables.JOB_STATUS.Done);
        wishlistJobInfo.setLastPullTime(new Date(System.currentTimeMillis()));

        // fetch the Wishlist records and push them to waiting table
        List<WishlistItemModel> newWishlistItemList = wishlistItemRepository
                .findAllByModifyDateAfter(lastPullOfferTime);
        logger.info("Fetched {} new Offer records, and set the last pull time to {}", newWishlistItemList.size(),
                DATE_FORMAT.format(wishlistJobInfo.getLastPullTime()));

        List<WishlistWaitingMatchModel> newWishlistWaitingRecordList = newWishlistItemList
                .parallelStream().map( newWishlistItem -> new WishlistWaitingMatchModel(
                        newWishlistItem.getWishlistItemId(), newWishlistItem.getStartTime()
                        , newWishlistItem.getEndTime(), newWishlistItem.getStateCityCode()))
                .toList();
        wishlistWaitingMatchRepository.saveAll(newWishlistWaitingRecordList);

        matchingJobInfoRepository.save(wishlistJobInfo);
        return newWishlistWaitingRecordList;
    }

    /**
     * This function will check records in the waiting table and try to match them. if match find, it will push them
     * into matched table.
     * @param matchJobRecord job info
     */
    private void findMatchFromWaitingTable(MatchingJobInfoModel matchJobRecord,
                                           List<OfferWaitingMatchModel> newOfferRecords,
                                           List<WishlistWaitingMatchModel> newWishlistRecords){
        logger.info("Starting findMatchFromWaitingTable function with lastPullTime:{}"
                ,DATE_FORMAT.format(matchJobRecord.getLastPullTime()));
        // update the job info record time
        Date lastPullOfferTime = matchJobRecord.getLastPullTime();
        matchJobRecord.setJobStatus(LookupTables.JOB_STATUS.Done);
        matchJobRecord.setLastPullTime(new Date(System.currentTimeMillis()));

        // if the list is null, fetch new records
        if(newOfferRecords == null)  newOfferRecords =
                offerWaitingMatchRepository.findAllByModifyDateAfter(lastPullOfferTime);
        if(newWishlistRecords == null) newWishlistRecords =
                wishlistWaitingMatchRepository.findAllByModifyDateAfter(lastPullOfferTime);

        // if one of them have new records
        if(newOfferRecords.size() > 0 || newWishlistRecords.size() > 0){
            // load new Offers into map

            ConcurrentMap<Long, List<OfferWaitingMatchModel>> newStateCityCodeOfferMap =
                    loadOfferListIntoMap(newOfferRecords);
            ConcurrentMap<Long, List<WishlistWaitingMatchModel>> newStateCityCodeWishlistMap =
                    loadWishlistListIntoMap(newWishlistRecords);

            // fetch both old records
            List<OfferWaitingMatchModel> oldOfferRecords =
                    offerWaitingMatchRepository.findAllByModifyDateLessThanEqualAndStateCityCodeIn(lastPullOfferTime,
                            newStateCityCodeWishlistMap.keySet());
            List<WishlistWaitingMatchModel> oldWishlistRecords =
                    wishlistWaitingMatchRepository.findAllByModifyDateLessThanEqualAndStateCityCodeIn(lastPullOfferTime,
                            newStateCityCodeOfferMap.keySet());

            // build maps by use the StateCityCode as the key so make the next pair step easier.

            ConcurrentMap<Long, List<OfferWaitingMatchModel>> oldStateCityCodeOfferMap =
                    loadOfferListIntoMap(oldOfferRecords);
            ConcurrentMap<Long, List<WishlistWaitingMatchModel>> oldStateCityCodeWishlistMap =
                    loadWishlistListIntoMap(oldWishlistRecords);

            // after fetch all new Wishlist item records by unique stateCityCode
            // match them with all Offer records
            newStateCityCodeWishlistMap.entrySet()
                    .parallelStream()
                    .forEach(entry -> {
                        List<OfferWaitingMatchModel> newStateCityCodeOfferList =
                                !newStateCityCodeOfferMap.containsKey(entry.getKey())? new ArrayList<>()
                                : newStateCityCodeOfferMap.get(entry.getKey());
                        List<OfferWaitingMatchModel> oldStateCityCodeOfferList =
                                !oldStateCityCodeOfferMap.containsKey(entry.getKey())? new ArrayList<>()
                                :oldStateCityCodeOfferMap.get(entry.getKey());
                        List<OfferWaitingMatchModel> offerList =
                                Stream.concat(newStateCityCodeOfferList.stream(),
                                        oldStateCityCodeOfferList.stream()).toList();
                        findMatchedOfferForNewWishlistItemToDB(offerList, entry.getValue());
                    });


            // If new offers and old Wishlist item records' unique stateCityCode has intersection,
            // fetch old Wishlist item by new Offer stateCityCode
            if(newOfferRecords.size() > 0){
                oldStateCityCodeWishlistMap.entrySet()
                        .parallelStream()
                        .forEach(entry ->{
                            List<OfferWaitingMatchModel> newStateCityCodeOfferList =
                                    !newStateCityCodeOfferMap.containsKey(entry.getKey())? new ArrayList<>()
                                            : newStateCityCodeOfferMap.get(entry.getKey());
                            findMatchedOfferForNewWishlistItemToDB(newStateCityCodeOfferList,
                                    entry.getValue());
                        });
            }

        }

        matchingJobInfoRepository.save(matchJobRecord);
    }


    private void findMatchedOfferForNewWishlistItemToDB(List<OfferWaitingMatchModel> offerList,
                                  List<WishlistWaitingMatchModel> wishlistItemList) {
        wishlistItemList.parallelStream().forEach(wishlistItem ->
            useWishlistItemAndOfferListBuildMatchedRecordDB(wishlistItem, offerList)
        );

    }

    private void useWishlistItemAndOfferListBuildMatchedRecordDB(WishlistWaitingMatchModel wishlistItem,
                                                               List<OfferWaitingMatchModel> offerList) {
        List<MatchedWishlistRecordModel> matchedRecordList = offerList.parallelStream()
                .filter(offer-> isWithin(wishlistItem.getStartTime(),wishlistItem.getEndTime(),
                        offer.getStartTime(),offer.getEndTime()) && checkAgreedOfferTime(offer, wishlistItem))
                .map(offer -> new MatchedWishlistRecordModel(offer.getOfferId(),
                        wishlistItem.getWishlistItemId(), LookupTables.MATCHING_RECORD_USER_RESULT.Waiting,
                        LookupTables.MATCHING_RECORD_USER_RESULT.Waiting,
                        wishlistItem.getStartTime(), wishlistItem.getEndTime()))
                .toList();
        matchedWishlistRecordRepository.saveAll(matchedRecordList);
    }

    private boolean checkAgreedOfferTime(OfferWaitingMatchModel offer, WishlistWaitingMatchModel wishlistItem) {
        AgreedRecordModel agreedRecordList = agreedRecordRepository
                .findOneByOfferIdAndStartTimeGreaterThanEqualAndStartTimeLessThanEqualOrOfferIdAndEndTimeGreaterThanEqualAndEndTimeLessThanEqual(
                        offer.getOfferId(),wishlistItem.getStartTime(), wishlistItem.getEndTime(),
                        offer.getOfferId(),wishlistItem.getStartTime(), wishlistItem.getEndTime());
        return agreedRecordList == null;
    }

    private ConcurrentMap<Long, List<OfferWaitingMatchModel>> loadOfferListIntoMap(
            List<OfferWaitingMatchModel> offerRecords){
        return offerRecords.parallelStream()
                .collect(Collectors.groupingByConcurrent(OfferWaitingMatchModel::getStateCityCode));
    }

    private ConcurrentMap<Long, List<WishlistWaitingMatchModel>> loadWishlistListIntoMap(
            List<WishlistWaitingMatchModel> wishlistRecords){
        return wishlistRecords.parallelStream()
                .collect(Collectors.groupingByConcurrent(WishlistWaitingMatchModel::getStateCityCode));
    }

    private boolean isOverlapping(Date start1, Date end1, Date start2, Date end2) {
        return start1.before(end2) && start2.before(end1);
    }

    private boolean isWithin(Date start1, Date end1, Date start2, Date end2){
        return start2.before(start1) && end1.before(end2);
    }

}
/*
Change logs:
Date        |       Author          |   Description
2022-10-30  |   Fangzheng Zhang     |   implemented match service
2022-11-03  |   Fangzheng Zhang     |   Optimised the code with parallel stream
 */