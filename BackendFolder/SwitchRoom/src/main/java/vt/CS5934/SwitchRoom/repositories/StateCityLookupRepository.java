package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.StateCityLookupModel;

public interface StateCityLookupRepository extends JpaRepository<StateCityLookupModel, Long> {
    StateCityLookupModel findByCityNameAndStateCode(String cityName, Integer stateCode);
    StateCityLookupModel findByStateCityCode(Long stateCityCode);
}
