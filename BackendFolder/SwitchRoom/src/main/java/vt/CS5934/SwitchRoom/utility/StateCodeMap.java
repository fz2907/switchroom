package vt.CS5934.SwitchRoom.utility;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class StateCodeMap {
    public static final Map<String, Integer> StringToCodeMap;
    static {
        StringToCodeMap = Map.ofEntries(Map.entry("AK", 0), Map.entry("AL", 1), Map.entry("AR", 2),
                Map.entry("AZ", 3), Map.entry("CA", 4), Map.entry("CO", 5), Map.entry("CT", 6),
                Map.entry("DE", 7), Map.entry("FL", 8), Map.entry("GA", 9), Map.entry("HI", 10),
                Map.entry("IA", 11), Map.entry("ID", 12), Map.entry("IL", 13),
                Map.entry("IN", 14), Map.entry("KS", 15), Map.entry("KY", 16),
                Map.entry("LA", 17), Map.entry("MA", 18), Map.entry("MD", 19),
                Map.entry("ME", 20), Map.entry("MI", 21), Map.entry("MN", 22),
                Map.entry("MO", 23), Map.entry("MS", 24), Map.entry("MT", 25),
                Map.entry("NC", 26), Map.entry("ND", 27), Map.entry("NE", 28),
                Map.entry("NH", 29), Map.entry("NJ", 30), Map.entry("NM", 31),
                Map.entry("NV", 32), Map.entry("NY", 33), Map.entry("OH", 34),
                Map.entry("OK", 35), Map.entry("OR", 36), Map.entry("PA", 37),
                Map.entry("RI", 38), Map.entry("SC", 39), Map.entry("SD", 40),
                Map.entry("TN", 41), Map.entry("TX", 42), Map.entry("UT", 43),
                Map.entry("VA", 44), Map.entry("VT", 45), Map.entry("WA", 46),
                Map.entry("WI", 47), Map.entry("WV", 48), Map.entry("WY", 49));
    }
}
