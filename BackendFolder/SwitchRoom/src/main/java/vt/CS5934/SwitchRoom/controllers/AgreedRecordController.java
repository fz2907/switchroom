package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.AgreedRecordModel;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.services.CommentRatingService;

@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("agreedRecord")
public class AgreedRecordController {
    private final Logger logger = LoggerFactory.getLogger(AgreedRecordController.class);

    @Autowired
    CommentRatingService commentRatingService;

    @GetMapping("/offer")
    public ResponseModel getAgreedOfferList(@CookieValue(value = "userId") Long userId){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(commentRatingService.getRecordsByOfferId(userId));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getWishlistList: "+e);
            responseModel.setMessage("getAgreedOfferList Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @GetMapping("/wishlist")
    public ResponseModel getAgreedWishlistList(@CookieValue(value = "userId") Long userId){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(commentRatingService.getRecordsByWishlistUserId(userId));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getWishlistList: "+e);
            responseModel.setMessage("getAgreedOfferList Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }

    @PostMapping("/updateCommentItem")
    public ResponseModel updateRecordCommentAndRating(@RequestBody AgreedRecordModel agreedRecordModel){
        ResponseModel responseModel = new ResponseModel();
        try{
            responseModel.setMessage("Success");
            responseModel.setStatus(HttpStatus.OK);
            responseModel.setData(commentRatingService.saveNewCommentAndRating(agreedRecordModel));
            return responseModel;
        }catch (Exception e){
            logger.error("Error in getWishlistList: "+e);
            responseModel.setMessage("updateRecordCommentAndRating Failed, Reason: " + e);
            responseModel.setStatus(HttpStatus.NOT_FOUND);
            responseModel.setData(null);
            return responseModel;
        }
    }
}
