package vt.CS5934.SwitchRoom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.models.AgreedRecordModel;
import vt.CS5934.SwitchRoom.repositories.AgreedRecordRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CommentRatingService {

    private final Logger logger = LoggerFactory.getLogger(CommentRatingService.class);
    @Autowired
    private AgreedRecordRepository agreedRecordRepository;

    public List<AgreedRecordModel> getRecordsByOfferId(Long OfferId){
        return agreedRecordRepository.findAllByOfferId(OfferId);
    }

    public List<AgreedRecordModel> getRecordsByWishlistUserId(Long wishlistUserId){
        return agreedRecordRepository.findAllByWishlistUserId(wishlistUserId);
    }

    public void setRecordDone(Long offerId, Long wishlistId){
        AgreedRecordModel record = agreedRecordRepository.findByOfferIdAndWishlistId(offerId, wishlistId);
        if(record!=null){
            record.setOnGoing(false);
            agreedRecordRepository.save(record);
        }else{
            logger.error("Can not find AgreedRecordModel with OfferId: {}, and wishlistId: {}", offerId, wishlistId);
        }
    }

    public AgreedRecordModel saveNewCommentAndRating(AgreedRecordModel agreedRecordModel){
        return agreedRecordRepository.save(agreedRecordModel);
    }
}
