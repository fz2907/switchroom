package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import vt.CS5934.SwitchRoom.models.InteractionModel;


public interface InteractionRepository extends JpaRepository<InteractionModel, Integer> {

    /**
     * The function name is the SQL query:
     *  findByIdAndName(long inputId, String inputName) == "SELETE * FROM table WHERE Id==inputId" AND name == inputName;
     * This is an example of how to declare a SQL command, those will be use in service class
     * @param userId the id in table you are looking for
     * @return ExampleModel object
     */
    InteractionModel findByUserId(long userId);

    InteractionModel findInteractionModelByOfferIdAndWishlistId(String offerId, String wishlistId);

    InteractionModel[] findInteractionModelsByUserId1(Integer userId);

    InteractionModel[] findInteractionModelsByUserId2(Integer userId);



}
