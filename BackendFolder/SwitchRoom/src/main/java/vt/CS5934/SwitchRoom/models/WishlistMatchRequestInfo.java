package vt.CS5934.SwitchRoom.models;

import lombok.Data;

import java.util.Date;

@Data
public class WishlistMatchRequestInfo {
    private Long wishlistOwnerId;
    private Long wishlistId;
    private Date startTime;
    private Date endTime;
    private String wishListOwnerName;
    private Boolean hasOffer;
    private Integer zipCode;

    public WishlistMatchRequestInfo(Long wishlistOwnerId, Long wishlistId, Date startTime, Date endTime,
                                    String wishListOwnerName, Boolean hasOffer, Integer zipCode) {
        this.wishlistOwnerId = wishlistOwnerId;
        this.wishlistId = wishlistId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.wishListOwnerName = wishListOwnerName;
        this.hasOffer = hasOffer;
        this.zipCode = zipCode;
    }

    public Long getWishlistOwnerId() {
        return wishlistOwnerId;
    }

    public void setWishlistOwnerId(Long wishlistOwnerId) {
        this.wishlistOwnerId = wishlistOwnerId;
    }

    public Long getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(Long wishlistId) {
        this.wishlistId = wishlistId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getWishListOwnerName() {
        return wishListOwnerName;
    }

    public void setWishListOwnerName(String wishListOwnerName) {
        this.wishListOwnerName = wishListOwnerName;
    }

    public Boolean getHasOffer() {
        return hasOffer;
    }

    public void setHasOffer(Boolean hasOffer) {
        this.hasOffer = hasOffer;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }
}
