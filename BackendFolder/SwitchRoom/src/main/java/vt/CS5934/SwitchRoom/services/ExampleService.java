/**
 * This class will hold the business logics of certain service
 * This class is a example service.
 */
package vt.CS5934.SwitchRoom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.models.ExampleModel;
import vt.CS5934.SwitchRoom.repositories.ExampleRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ExampleService {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(ExampleService.class);

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    ExampleRepository exampleRepository;

    public ExampleModel addNewExampleModelToDB(String username){
        logger.info("Reached addNewExampleModelToDB()");
        ExampleModel newModel = new ExampleModel(username);
        exampleRepository.save(newModel);
        return newModel;
    }

    public List<ExampleModel> getAllExampleModelsFromDB(){
        logger.info("Reached getAllExampleModelsFromDB()");
        return exampleRepository.findAll();
    }

    public ExampleModel getExampleModelWithIDFromDB(long id){
        logger.info("Reached getExampleModelWithIDFromDB()");
        return exampleRepository.findByUserId(id);
    }
    @Transactional
    public boolean removeExampleModelWithId(long id){
        logger.info("Reached removeExampleModelWithId()");
        exampleRepository.deleteByUserId(id);
        return true;
    }

}
/*
Change logs:
Date        |       Author          |       Description
2022-10-15  |   Fangzheng Zhang     |   Set up example service class
 */