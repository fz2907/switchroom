package vt.CS5934.SwitchRoom.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.bytebuddy.implementation.bytecode.Throw;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.hash.SHAModel;
import vt.CS5934.SwitchRoom.models.ExampleModel;
import vt.CS5934.SwitchRoom.models.ResponseModel;
import vt.CS5934.SwitchRoom.models.UserModel;
import vt.CS5934.SwitchRoom.repositories.UserRepository;
import vt.CS5934.SwitchRoom.utility.UsefulTools;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.mail.javamail.JavaMailSender;
import vt.CS5934.SwitchRoom.utility.UsefulTools;


@Service
public class UserService {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    private SHAModel shaModel = new SHAModel();

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    UserRepository userRepository;

    public UserService() throws NoSuchAlgorithmException {
    }

    public UserModel addUserToDB(String username, String password, String email, String firstname, String lastname, String gender){
        logger.info("Reached addNewExampleModelToDB()");
        UserModel newUser = new UserModel(username, password, email, firstname, lastname, gender, null, null);
        userRepository.save(newUser);
        return newUser;
    }

    public String[] loginUser(UserModel userInfo) throws NoSuchAlgorithmException {
        String username = userInfo.getUsername();
        String password = hashPassword(userInfo.getPassword());
        UserModel existUser = userRepository.findByUsername(username);

        if (existUser == null) {
            logger.error("loginUser: FORBIDDEN (user not found)");
            return new String[] {};
        } else if (existUser.getPassword().equals(password)) {
            var token = Token.of(existUser.getUserId(), 10L, "secret");
            existUser.setToken(token.getToken());
            userRepository.save(existUser);

            String result[] = new String[2];
            result[0] = existUser.getUserId().toString();
            result[1] = token.getToken();
            return result;
        } else {
            logger.warn("loginUser: FORBIDDEN (wrong password)");
            return new String[] {};
        }
    }

    public boolean checkLoginSession(String userId, String token) {
        UserModel existUser = userRepository.findByUserId(Integer.parseInt(userId));
        if (existUser == null) {
            logger.error("checkLoginSession: FORBIDDEN (user not found)");
            return false;
        } else if (existUser.getToken().equals(token)) {
            return true;
        } else {
            logger.error("checkLoginSession: FORBIDDEN (invalid token)");
            return false;
        }
    }

    public String hashPassword(String password) throws NoSuchAlgorithmException {
        String result = "";
        result = shaModel.toHexString(shaModel.getSHA(password));
        return result;
    }

    public boolean resetPassword(String userId, String payload) throws NoSuchAlgorithmException, JsonProcessingException {
        UserModel existUser = userRepository.findByUserId(Integer.parseInt(userId));

        if (existUser == null) {
            logger.warn("checkLoginSession: FORBIDDEN (user not found)");
            return false;
        }
        ObjectMapper mapper = new ObjectMapper();
        String oldPassword = mapper.readTree(payload)
                .get("oldPassword")
                .asText();
        String newPassword = mapper.readTree(payload)
                .get("newPassword")
                .asText();
        String inputOldPassword = hashPassword(oldPassword);

        if (existUser.getPassword().equals(inputOldPassword)) {
            String inputNewPassword = hashPassword(newPassword);
            existUser.setPassword(inputNewPassword);
            userRepository.save(existUser);
            return true;
        } else {
            logger.warn("checkLoginSession: FORBIDDEN (password not matched)");
            return false;
        }
    }

    public ResponseModel getProfile(String userId) {
        ResponseModel response = new ResponseModel();
        UserModel existUser = userRepository.findByUserId(Integer.parseInt(userId));

        if (existUser == null) {
            logger.warn("getProfile: FORBIDDEN (user not found)");
            response.setMessage("user not found");
            response.setStatus(HttpStatus.FORBIDDEN);
            return response;
        }

        Map<String, String> profile = new LinkedHashMap<>();
        profile.put("username", existUser.getUsername());
        profile.put("email", existUser.getEmail());
        profile.put("firstName", existUser.getFirstname());
        profile.put("lastName", existUser.getLastname());

        response.setData(profile);
        response.setStatus(HttpStatus.OK);

        return response;
    }

    public boolean updateResetPasswordToken(String payload, HttpServletRequest request) throws MessagingException, UnsupportedEncodingException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String email = mapper.readTree(payload)
                .get("email")
                .asText();
        UserModel existUser = userRepository.findByEmail(email);
        if (existUser != null) {
            String token = Token.of(existUser.getUserId(), 10L, "secret").getToken();
            existUser.setResetPasswordToken(token);
            userRepository.save(existUser);
            String resetPasswordLink = UsefulTools.getSiteURL(request) + "/user/forgotPassword_verify?token=" + token;
            sendEmail(email, resetPasswordLink, existUser.getUsername());
            return true;
        } else {
            logger.warn("checkLoginSession: FORBIDDEN (user not found)");
            return false;
        }
    }

    public void sendEmail(String recipientEmail, String link, String username)
            throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("switchroomvt@gmail.com", "Switch Room Support");
        helper.setTo(recipientEmail);

        String subject = "(Switch Room Support) Here's the link to reset your password";

        String content = "<p>Hello, " + username + "</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        helper.setSubject(subject);

        helper.setText(content, true);

        mailSender.send(message);
    }

    public UserModel getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    public boolean forgotPasswordCreateNew(String token, String payload) throws NoSuchAlgorithmException, JsonProcessingException {
        UserModel existUser = getByResetPasswordToken(token);
        if (existUser != null) {
            ObjectMapper mapper = new ObjectMapper();
            String password = mapper.readTree(payload)
                    .get("password")
                    .asText();
            String encodedPassword = hashPassword(password);
            existUser.setPassword(encodedPassword);

            existUser.setResetPasswordToken(null);
            userRepository.save(existUser);
            return true;
        } else {
            return false;
        }
    }

    public UserModel findSpecificUser(int userId) {
        UserModel specificUser = userRepository.findByUserId(userId);
        return specificUser;
    }
}
