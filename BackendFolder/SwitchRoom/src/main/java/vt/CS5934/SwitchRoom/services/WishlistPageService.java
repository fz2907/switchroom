/**
 * This class will handle all the service related to wishlist page
 */
package vt.CS5934.SwitchRoom.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vt.CS5934.SwitchRoom.models.*;
import vt.CS5934.SwitchRoom.repositories.*;
import vt.CS5934.SwitchRoom.utility.StateCodeMap;

import java.util.ArrayList;
import java.util.List;

@Service
public class WishlistPageService {

    @Autowired
    WishlistItemRepository wishlistItemRepository;
    @Autowired
    UserOfferWishlistLookUpRepository userOfferWishlistLookUpRepository;
    @Autowired
    StateCityLookupRepository stateCityLookupRepository;
    @Autowired
    MatchedWishlistRecordRepository matchedWishlistRecordRepository;
    @Autowired
    WishlistWaitingMatchRepository wishlistWaitingMatchRepository;
    @Autowired
    UserOfferRepository userOfferRepository;
    @Autowired
    UserRepository userRepository;

    /**
     * This function will take userId and look into UserOfferWishlistLookUpModel DB and find all the wishlist item id
     * then fetch the items from WishlistItemModel DB as a list
     * @param userId userId
     * @return A list of matching WishlistItemModels
     */
    public List<WishlistItemModel> getWishlistList(Long userId) {
        List<UserOfferWishlistLookUpModel> lookUpModels = userOfferWishlistLookUpRepository.findAllByUserId(userId);
        List<WishlistItemModel> ls = new ArrayList<>();
        for(UserOfferWishlistLookUpModel model : lookUpModels){
            ls.add(wishlistItemRepository.findByWishlistItemId(model.getWishlistItemId()));
        }
        return ls;
    }

    /**
     * This function will save the WishlistItemModel into its DB first to get the wishlistItemID
     * Then add the userId with wishlistItemID record into UserOfferWishlistLookUpModel DB
     * @param userId userId
     * @param wishlistItemModel new wishlistItemModel without wishlistItemID
     * @return saved wishlistItemModel with wishlistItemID
     */
    @Transactional
    public WishlistItemModel saveNewWishlistItem(Long userId, WishlistItemModel wishlistItemModel) {
        fillStateCodeAndStateCityCode(wishlistItemModel);
        WishlistItemModel wishlistItemModelWithId = wishlistItemRepository.save(wishlistItemModel);
        UserOfferWishlistLookUpModel lookUpModel = new UserOfferWishlistLookUpModel(userId,
                wishlistItemModelWithId.getWishlistItemId());
        userOfferWishlistLookUpRepository.save(lookUpModel);
        return wishlistItemModelWithId;
    }

    /**
     * This function will delete the wishlistItemModel from its DB by wishlistItemID
     * Then delete the match record from UserOfferWishlistLookUpModel DB by wishlistItemID
     * @param wishlistItemId wishlistItemID gaven by user
     */
    @Transactional
    public void deleteWishlistItem(Long wishlistItemId) {
        wishlistItemRepository.deleteByWishlistItemId(wishlistItemId);
        userOfferWishlistLookUpRepository.deleteByWishlistItemId(wishlistItemId);
        wishlistWaitingMatchRepository.deleteAllByWishlistItemId(wishlistItemId);
        matchedWishlistRecordRepository.deleteAllByWishlistItemId(wishlistItemId);
    }

    /**
     * This function will update to exist wishlistItemModel in its table
     * @param wishlistItemModel user given wishlistItemModel with wishlistItemID
     * @return changed wishlistItemModel in DB
     */
    @Transactional
    public WishlistItemModel updateWishlistItem(WishlistItemModel wishlistItemModel) {
        fillStateCodeAndStateCityCode(wishlistItemModel);
        matchedWishlistRecordRepository.deleteAllByWishlistItemId(wishlistItemModel.getWishlistItemId());
        return wishlistItemRepository.save(wishlistItemModel);
    }

    public WishlistItemModel getWishlistItemInfo(Long wishlistItemId) {
        return wishlistItemRepository.findByWishlistItemId(wishlistItemId);
    }

    public Long getPairedUserId( Long wishlistItemId ){
        return userOfferWishlistLookUpRepository.findByWishlistItemId(wishlistItemId).getUserId();
    }


    /**
     * This function will take a UserOfferModel. it will fill the StateCode with it first,
     * then check and update the StateCityCode. If DB do not have the StateCityCode, then build one and fill it in.
     * @param wishlistItemModel the new or updated offerModel
     */
    private void fillStateCodeAndStateCityCode(WishlistItemModel wishlistItemModel){

        // Fill state code in the record
        wishlistItemModel.setStateCode(StateCodeMap.StringToCodeMap.get(wishlistItemModel.getState()));

        // Check if the DB have the state city code or not, if not build one
        StateCityLookupModel stateCityLookupModel = stateCityLookupRepository
                .findByCityNameAndStateCode(wishlistItemModel.getCityName(), wishlistItemModel.getStateCode());

        if(stateCityLookupModel == null){
            // not in DB yet
            stateCityLookupModel = stateCityLookupRepository.save(
                    new StateCityLookupModel(null, wishlistItemModel.getStateCode(),
                            wishlistItemModel.getCityName()));
        }
        wishlistItemModel.setStateCityCode(stateCityLookupModel.getStateCityCode());
    }

    /**
     * This function is used to get the matched offers of the given wishlist item
     * @param wishlistItemId the target id
     * @return a list of matched offer info of the given wishlistItem
     */
    public List<OfferMatchRequestInfo> getOfferMatchList(Long wishlistItemId) {
        List<OfferMatchRequestInfo> offerMatchRequestInfoList = new ArrayList<>();
        List<MatchedWishlistRecordModel> matchedRecordList =
                matchedWishlistRecordRepository.findAllByWishlistItemId(wishlistItemId);
        matchedRecordList.forEach( matchedRecord ->{
            UserOfferModel userOfferModel = userOfferRepository.findByUserId(matchedRecord.getOfferId());
            UserModel userInfo = userRepository.findByUserId(userOfferModel.getUserId().intValue());
            offerMatchRequestInfoList.add(new OfferMatchRequestInfo(userOfferModel.getUserId(),
                    wishlistItemId, userOfferModel.getAvailableTimeStart(),userOfferModel.getAvailableTimeEnd(),
                    userOfferModel.getZipCode(), userInfo.getFirstname()+" "+ userInfo.getLastname()));
        });
        return offerMatchRequestInfoList;
    }

    public List<Long> getOfferMatchCount(List<Long> wishlistItemIdList) {
        List<Long> countList = new ArrayList<>();
        wishlistItemIdList.forEach((wishListId ->{
            Long count = matchedWishlistRecordRepository.countByWishlistItemId(wishListId);
            countList.add(count == null ? 0 : count);
        }));
        return countList;
    }
}
/*
Change logs:
Date        |       Author          |   Description
2022-10-21  |    Fangzheng Zhang    |    create class and init
2022-10-30  |   Fangzheng Zhang     |   implement state city code feature
2022-11-03  |   Fangzheng Zhang     |   handle update and deletion to matched table and waiting table
2022-11-05  |   Fangzheng Zhang     |   Add getOfferMatchList function
 */