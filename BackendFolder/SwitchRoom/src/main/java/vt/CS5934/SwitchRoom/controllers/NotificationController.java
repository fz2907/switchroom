package vt.CS5934.SwitchRoom.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vt.CS5934.SwitchRoom.models.*;
import vt.CS5934.SwitchRoom.services.DealWithWishlist;
import vt.CS5934.SwitchRoom.services.MatchedWishlistRecordService;
import vt.CS5934.SwitchRoom.services.OfferWishlistLookUpService;
import vt.CS5934.SwitchRoom.services.UserService;

import java.util.List;


/**
 * The "@RestController" made the class into rest handle class
 * The "@RequestMapping("example")" on the class level make it only react to url ".../example/..."
 */
@CrossOrigin(
        allowCredentials = "true",
        origins = {"http://localhost:8080/"}
)
@RestController
@RequestMapping("notification")
public class NotificationController {
    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    MatchedWishlistRecordService matchedWishlistRecordService;

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    OfferWishlistLookUpService offerWishlistLookUpService;
    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    DealWithWishlist dealWithWishlist;

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * This function will handle the post function and change the JSON body into data class
     */
    @CrossOrigin
    @GetMapping("/{userId}")
    public ResponseModel getNotification(@PathVariable long userId) {
        logger.info("You reached the getNotification() function.");
//        System.out.println(userId);
        try{
            List<MatchedWishlistRecordModel> offerNotifications = matchedWishlistRecordService.getOfferListWithIDFromDB(userId);
//            System.out.println(offerNotifications);
//            List<ExampleModel> exampleModels = exampleService.getAllExampleModelsFromDB();
            return new ResponseModel(
                    "This this a notification String response",
                    HttpStatus.OK,
                    offerNotifications);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get All ExampleModels, Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }

    /**
     * This function will handle the post function and change the JSON body into data class
     */
    @CrossOrigin(
            allowCredentials = "true",
            origins = {"http://localhost:8080/"}
    )
    @GetMapping("/wish/{userId}")
    public ResponseModel getNotificationwish(@PathVariable long userId) {
        logger.info("You reached the getNotificationwish() function.");
//        System.out.println(userId);
        try{
            List<UserOfferWishlistLookUpModel> lookup = offerWishlistLookUpService.getOfferWishlistLookUpWithIDFromDB(userId);
            List<MatchedWishlistRecordModel> wishlists = dealWithWishlist.getMatchedWishlist(lookup);
//            List<MatchedWishlistRecordModel> offerNotifications = matchedWishlistRecordService.getOfferListWithIDFromDB(userId);
//            List<ExampleModel> exampleModels = exampleService.getAllExampleModelsFromDB();
            return new ResponseModel(
                    "This this a notification String response",
                    HttpStatus.OK,
                    wishlists);
        }catch (Exception e){
            return new ResponseModel(
                    "Error occur on get All ExampleModels, Error info is: " + e,
                    HttpStatus.OK,
                    null);
        }
    }
}
