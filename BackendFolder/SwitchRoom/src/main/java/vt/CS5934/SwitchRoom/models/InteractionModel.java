package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * '@Data' tell spring this class can be map to JSON object if needed
 * '@Entity' declare this class is a DB table
 * '@Table(name=<table_name>)' declared which table stores its data
 */
@Data
@Entity
@Table(name = "Interaction")
@NoArgsConstructor
public class InteractionModel {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;
    @Column(name="user_id1")
    private Integer userId1;

    @Column(name="user_id2")
    private Integer userId2;

    @Column(name="phone1")
    private String phone1;

    @Column(name="phone2")
    private String phone2;

    @Column(name="judgement")
    private Boolean judgement;

    @Column(name="offerId")
    private String offerId;

    @Column(name="wishlistId")
    private String wishlistId;


    public InteractionModel(Integer userId1, Integer userId2, String phone1, String phone2, Boolean judgement, String offerId, String wishlistId) {
        this.userId1 = userId1;
        this.userId2 = userId2;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.judgement = judgement;
        this.offerId = offerId;
        this.wishlistId = wishlistId;
    }

    @Override
    public String toString() {
        return "InteractionModel{" +
                "userId1=" + userId1 +
                ", userId2=" + userId2 +
                ", phone1='" + phone1 + '\'' +
                ", phone2='" + phone2 + '\'' +
                ", judgement=" + judgement + '\'' +
                ", offerId=" + offerId + '\'' +
                ", wishlistId=" + wishlistId +
                '}';
    }
}
