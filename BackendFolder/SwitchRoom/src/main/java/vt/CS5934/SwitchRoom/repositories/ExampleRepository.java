/**
 * This is a repository interface.
 * The real class object will be build by SpringBoot to make sure only
 *  one connection to DB is allowed.
 *  You can build you query in this class then use it later.
 */

package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.ExampleModel;

import java.util.List;

/**
 * The interface have to extend JpaRepository, and the type of the class should be
 * < The_model_class, Table_key(@id)>. In this example, the model class is ExampleModel
 */
public interface ExampleRepository extends JpaRepository<ExampleModel, Long> {

    /**
     * The function name is the SQL query:
     *  findByIdAndName(long inputId, String inputName) == "SELETE * FROM table WHERE Id==inputId" AND name == inputName;
     * This is an example of how to declare a SQL command, those will be use in service class
     * @param userId the id in table you are looking for
     * @return ExampleModel object
     */
    ExampleModel findByUserId(long userId);

    List<ExampleModel> findAll();
    void deleteByUserId(long userId);
}


/*
Change logs:
Date        |       Author          |       Description
2022-10-15  |   Fangzheng Zhang     |   Set up example repository for example mode table
 */