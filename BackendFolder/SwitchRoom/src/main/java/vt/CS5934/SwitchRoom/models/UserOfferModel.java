package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "user_offer_table")
@NoArgsConstructor
public class UserOfferModel {

    @Id
    private Long userId;
    private String state;
    private Integer stateCode;
    private Integer zipCode;
    private String spaceType;
    private String otherSpaceType;
    private String spaceLocateCity;
    private Long stateCityCode;
    private Date availableTimeStart;
    private Date availableTimeEnd;
    private Integer maxNumberOfPeople;
    @Column(length = 500)
    private String spaceDetails;
    private Boolean offering;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private java.util.Date modifyDate;


    public UserOfferModel(Long userId, String state, Integer stateCode, Integer zipCode, String spaceType,
                          String otherSpaceType, String spaceLocateCity,
                          Long cityCode, Date availableTimeStart, Date availableTimeEnd,
                          Integer maxNumberOfPeople, String spaceDetails, Boolean offering) {
        this.userId = userId;
        this.state = state;
        this.stateCode = stateCode;
        this.zipCode = zipCode;
        this.spaceType = spaceType;
        this.otherSpaceType = otherSpaceType;
        this.spaceLocateCity = spaceLocateCity;
        this.stateCityCode = cityCode;
        this.availableTimeStart = availableTimeStart;
        this.availableTimeEnd = availableTimeEnd;
        this.maxNumberOfPeople = maxNumberOfPeople;
        this.spaceDetails = spaceDetails;
        this.offering = offering;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getStateCode() {
        return stateCode;
    }

    public void setStateCode(Integer stateCode) {
        this.stateCode = stateCode;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getSpaceType() {
        return spaceType;
    }

    public void setSpaceType(String spaceType) {
        this.spaceType = spaceType;
    }

    public String getOtherSpaceType() {
        return otherSpaceType;
    }

    public void setOtherSpaceType(String otherSpaceType) {
        this.otherSpaceType = otherSpaceType;
    }

    public String getSpaceLocateCity() {
        return spaceLocateCity;
    }

    public void setSpaceLocateCity(String spaceLocateCity) {
        this.spaceLocateCity = spaceLocateCity;
    }

    public Long getStateCityCode() {
        return stateCityCode;
    }

    public void setStateCityCode(Long cityCode) {
        this.stateCityCode = cityCode;
    }

    public Date getAvailableTimeStart() {
        return availableTimeStart;
    }

    public void setAvailableTimeStart(Date availableTimeStart) {
        this.availableTimeStart = availableTimeStart;
    }

    public Date getAvailableTimeEnd() {
        return availableTimeEnd;
    }

    public void setAvailableTimeEnd(Date availableTimeEnd) {
        this.availableTimeEnd = availableTimeEnd;
    }

    public Integer getMaxNumberOfPeople() {
        return maxNumberOfPeople;
    }

    public void setMaxNumberOfPeople(Integer maxNumberOfPeople) {
        this.maxNumberOfPeople = maxNumberOfPeople;
    }

    public String getSpaceDetails() {
        return spaceDetails;
    }

    public void setSpaceDetails(String spaceDetails) {
        this.spaceDetails = spaceDetails;
    }

    public Boolean getOffering() {
        return offering;
    }

    public void setOffering(Boolean offering) {
        this.offering = offering;
    }

}
/*
Change logs:
Date        |       Author          |       Description
2022-10-20  |   Fangzheng Zhang     |    create class and init
2022-10-30  |   Fangzheng Zhang     |   Add modify_date into DB
 */