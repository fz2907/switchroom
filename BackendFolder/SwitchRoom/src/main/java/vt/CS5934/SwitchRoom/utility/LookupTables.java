package vt.CS5934.SwitchRoom.utility;

public class LookupTables {
    // Matching job service related fields
    public static final Long OFFER_JOB_DB_ID = 1L;
    public static final Long WISHLIST_JOB_DB_ID = 2L;
    public static final Long MATCHING_JOB_DB_ID = 3L;

    // Matching related DB data fields
    public enum JOB_STATUS {Running, Done, Stopped}
    public enum MATCHING_RECORD_USER_RESULT {Waiting, Accepted, Declined}

}
