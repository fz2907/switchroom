package vt.CS5934.SwitchRoom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vt.CS5934.SwitchRoom.models.MatchingJobInfoModel;

public interface MatchingJobInfoRepository extends JpaRepository<MatchingJobInfoModel, Long> {
    MatchingJobInfoModel findByJobId(Long jobId);
}
