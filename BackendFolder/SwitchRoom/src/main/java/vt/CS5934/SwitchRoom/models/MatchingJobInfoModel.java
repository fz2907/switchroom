package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;
import vt.CS5934.SwitchRoom.utility.LookupTables;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "matching_job_info_table")
@NoArgsConstructor
public class MatchingJobInfoModel {

    @Id
    private Long jobId;
    private LookupTables.JOB_STATUS jobStatus;
    private java.util.Date lastPullTime;
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private java.util.Date modifyDate;

    public MatchingJobInfoModel(Long jobId, LookupTables.JOB_STATUS jobStatus, Date lastPullTime) {
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.lastPullTime = lastPullTime;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public LookupTables.JOB_STATUS getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(LookupTables.JOB_STATUS jobStatus) {
        this.jobStatus = jobStatus;
    }

    public java.util.Date getLastPullTime() {
        return lastPullTime;
    }

    public void setLastPullTime(java.util.Date lastStartTime) {
        this.lastPullTime = lastStartTime;
    }

    public java.util.Date getModifyDate() {
        return modifyDate;
    }

}
