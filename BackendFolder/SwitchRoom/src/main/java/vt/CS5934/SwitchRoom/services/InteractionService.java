package vt.CS5934.SwitchRoom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vt.CS5934.SwitchRoom.models.InteractionCombinedModel;
import vt.CS5934.SwitchRoom.models.InteractionModel;
import vt.CS5934.SwitchRoom.models.UserModel;
import vt.CS5934.SwitchRoom.repositories.InteractionRepository;
import vt.CS5934.SwitchRoom.repositories.UserRepository;

@Service
public class InteractionService {

    /**
     * You can use logger.[trace,debug,info,warn,error]("messages") to log into file
     */
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * Autowired is a Spring feature that it will create or looking for the existing object in memory.
     * It usually uses on Repository class, Service class, or some globe object in the class.
     */
    @Autowired
    InteractionRepository interactionRepository;

    @Autowired
    UserRepository userRepository;


    public InteractionModel addDataToDB(Integer userId1, Integer userId2, String phone1, String phone2, Boolean judgement, String offerId, String wishlistId){
        logger.info("Reached addNewExampleModelToDB()");
        InteractionModel data = new InteractionModel(userId1, userId2, phone1, phone2, judgement, offerId, wishlistId);
        interactionRepository.save(data);
        return data;
    }

    public InteractionModel searchBothIdFromDB(String offerId, String wishlistId) {
        logger.info("Reached searchBothIdFromDB()");
        InteractionModel result = interactionRepository.findInteractionModelByOfferIdAndWishlistId(offerId, wishlistId);
        return result;
    }

    public InteractionModel updateToDB(InteractionModel updateData) {
        interactionRepository.save(updateData);
        return updateData;
    }

    public InteractionCombinedModel findAllInteractionsFromDB(Integer userId) {
        InteractionModel[] res1 = interactionRepository.findInteractionModelsByUserId1(userId);
        InteractionModel[] res2 = interactionRepository.findInteractionModelsByUserId2(userId);
        UserModel[] user1 = new UserModel[res1.length];
        UserModel[] user2 = new UserModel[res2.length];
        for(int i = 0; i < res1.length; i++) {
            UserModel user = userRepository.findByUserId(res1[i].getUserId2());
            user1[i] = user;
        }
        for(int j = 0; j < res2.length; j++) {
            UserModel user = userRepository.findByUserId(res2[j].getUserId1());
            user2[j] = user;
        }

        InteractionCombinedModel res = new InteractionCombinedModel(res1, res2, user1, user2);
//        int fal = res1.length;
//        int sal = res2.length;
//        InteractionModel[] res = new InteractionModel[fal + sal];
//        System.arraycopy(res1, 0, res, 0, fal);
//        System.arraycopy(res2, 0, res, fal, sal);
        return res;
    }
}
