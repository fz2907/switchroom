package vt.CS5934.SwitchRoom.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vt.CS5934.SwitchRoom.models.*;
import vt.CS5934.SwitchRoom.repositories.*;
import vt.CS5934.SwitchRoom.utility.StateCodeMap;

import java.util.ArrayList;
import java.util.List;

@Service
public class OfferPageService {
    private final Logger logger = LoggerFactory.getLogger(OfferPageService.class);

    @Autowired
    UserOfferRepository userOfferRepository;
    @Autowired
    StateCityLookupRepository stateCityLookupRepository;
    @Autowired
    MatchedWishlistRecordRepository matchedWishlistRecordRepository;
    @Autowired
    OfferWaitingMatchRepository offerWaitingMatchRepository;
    @Autowired
    UserOfferWishlistLookUpRepository userOfferWishlistLookUpRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    WishlistItemRepository wishlistItemRepository;

    public UserOfferModel getUserOfferInfo(Long userId) {
        UserOfferModel userOfferModel = userOfferRepository.findByUserId(userId);
        if(userOfferModel == null) {
            userOfferModel = new UserOfferModel();
            userOfferModel.setUserId(userId);
        }
        return userOfferModel;
    }


    public UserOfferModel saveNewUserOffer(UserOfferModel offerModel) {
        fillStateCodeAndStateCityCode(offerModel);
        return userOfferRepository.save(offerModel);
    }
    @Transactional
    public void removeOfferFromMatchedTable (Long offerId){
        logger.info("Removing all records related to offerId: {} from matched table.", offerId);
        matchedWishlistRecordRepository.deleteAllByOfferId(offerId);
    }

    @Transactional
    public void deleteUserOffer(Long userId) {
        logger.info("Removing all records related to offerId: {} from offer waiting table.", userId);
        userOfferRepository.deleteByUserId(userId);
        offerWaitingMatchRepository.deleteAllByOfferId(userId);
        removeOfferFromMatchedTable(userId);
    }




    /**
     * This function will take a UserOfferModel. it will fill the StateCode with it first,
     * then check and update the StateCityCode. If DB do not have the StateCityCode, then build one and fill it in.
     * @param offerModel the new or updated offerModel
     */
    private void fillStateCodeAndStateCityCode(UserOfferModel offerModel){

        // Fill state code in the record
        offerModel.setStateCode(StateCodeMap.StringToCodeMap.get(offerModel.getState()));

        // Check if the DB have the state city code or not, if not build one
        StateCityLookupModel stateCityLookupModel = stateCityLookupRepository
                .findByCityNameAndStateCode(offerModel.getSpaceLocateCity(), offerModel.getStateCode());

        if(stateCityLookupModel == null){
            // not in DB yet
            stateCityLookupModel = stateCityLookupRepository.save(
                    new StateCityLookupModel(null, offerModel.getStateCode(),
                            offerModel.getSpaceLocateCity()));
        }
        offerModel.setStateCityCode(stateCityLookupModel.getStateCityCode());
    }

    /**
     * This service will fetch information from many table to get the WishlistMatchRequestInfo, so we can display it in
     * the offer page
     * @param userId The id of user who opened the offer page
     * @return a list of WishlistMatchRequestInfo.
     */
    public List<WishlistMatchRequestInfo> getWishlistMatchRequestInfoList(Long userId) {
        List<WishlistMatchRequestInfo> wishlistMatchRequestInfoList = new ArrayList<>();
        List<MatchedWishlistRecordModel> matchedRecordList = matchedWishlistRecordRepository.findAllByOfferId(userId);
        for(MatchedWishlistRecordModel matchedRecord : matchedRecordList){
            Long requestedWishlistId = matchedRecord.getWishlistItemId();
            Long wishlistOwnerId = userOfferWishlistLookUpRepository
                    .findByWishlistItemId(requestedWishlistId).getUserId();
            // get user information from userRepository, get wishlistItem information from wishlistItem table,
            //  check if the wishlistItem owner has Offer or not, if does, find zipcode.
            UserModel wishlistOwnerInfo = userRepository.findByUserId(wishlistOwnerId.intValue());
            WishlistItemModel wishlistItem = wishlistItemRepository.findByWishlistItemId(requestedWishlistId);
            UserOfferModel wishlistOwnerOfferModel = userOfferRepository.findByUserId(wishlistOwnerId);
            WishlistMatchRequestInfo wishlistMatchRequestInfo = new WishlistMatchRequestInfo(wishlistOwnerId,
                    requestedWishlistId, wishlistItem.getStartTime(), wishlistItem.getEndTime(),
                    wishlistOwnerInfo.getFirstname() + " " + wishlistOwnerInfo.getLastname(),
                    wishlistOwnerOfferModel != null && wishlistOwnerOfferModel.getOffering(),
                    wishlistOwnerOfferModel == null ? null : wishlistOwnerOfferModel.getZipCode());
            wishlistMatchRequestInfoList.add(wishlistMatchRequestInfo);
        }
        return wishlistMatchRequestInfoList;
    }
}
/*
Change logs:
Date        |       Author          |   Description
2022-10-21  |    Fangzheng Zhang    |    create class and init
2022-10-30  |   Fangzheng Zhang     |   implement state city code feature
2022-11-03  |   Fangzheng Zhang     |   Added update and remove handling for matching waiting table and matched table

 */