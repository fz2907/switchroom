/**
 * Model for wishlist item table
 */
package vt.CS5934.SwitchRoom.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "wishlist_item_table")
@NoArgsConstructor
public class WishlistItemModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long wishlistItemId;
    private String cityName;
    private Long stateCityCode;
    private Integer zipCode;
    private String state;
    private Integer stateCode;
    private Date startTime;
    private Date endTime;
    @Column(length = 500)
    private String details;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modify_date")
    private java.util.Date modifyDate;

    public WishlistItemModel(Long wishlistItemId, String cityName, Long stateCityCode, String state, Integer stateCode,
                             Date startTime, Date endTime, String details, Integer zipCode) {
        this.wishlistItemId = wishlistItemId;
        this.cityName = cityName;
        this.stateCityCode = stateCityCode;
        this.zipCode = zipCode;
        this.state = state;
        this.stateCode = stateCode;
        this.startTime = startTime;
        this.endTime = endTime;
        this.details = details;
    }

    public WishlistItemModel(String cityName, Long stateCityCode, String state, Integer stateCode, Date startTime,
                             Date endTime, String details) {
        this.cityName = cityName;
        this.stateCityCode = stateCityCode;
        this.state = state;
        this.stateCode = stateCode;
        this.startTime = startTime;
        this.endTime = endTime;
        this.details = details;
    }

    public Long getWishlistItemId() {
        return wishlistItemId;
    }

    public Long getStateCityCode() {
        return stateCityCode;
    }

    public void setStateCityCode(Long stateCityCode) {
        this.stateCityCode = stateCityCode;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public void setWishlistItemId(Long wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getStateCode() {
        return stateCode;
    }

    public void setStateCode(Integer stateCode) {
        this.stateCode = stateCode;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}

/*
Change logs:
Date        |       Author          |   Description
2022-10-21  |    Fangzheng Zhang    |    create class and init
2022-10-30  |   Fangzheng Zhang     |   Add modify_date into DB

 */